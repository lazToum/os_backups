#!/bin/bash
set -e

if [[ "$(whoami)" != "root" ]]
  then echo "Run as root or with sudo"
  exit 1
fi

HERE=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
cd ${HERE}
if [[ ! -f ${HERE}/.env ]];then
  echo "Please include a .env file containing the SSH_KEYS_USER, and SSH_PORT variables : SSH_KEYS_USER=......"
  exit 1
fi

. ${HERE}/.env

DISTRO=""

SSH_KEYS_USER=${SSH_KEYS_USER:-keyman}  # has to be the same user with the one in the backup server
SSH_PORT="${SSH_PORT:-22}"

if ! id "${SSH_KEYS_USER}" >/dev/null 2>&1; then
    useradd -d /home/${SSH_KEYS_USER} -s /bin/bash ${SSH_KEYS_USER}
fi

if [[ ! -d /home/${SSH_KEYS_USER}/.ssh ]]; then
  mkdir -p /home/${SSH_KEYS_USER}/.ssh
fi

if [[ ! -f /home/${SSH_KEYS_USER}/.ssh/authorized_keys ]]; then
  touch /home/${SSH_KEYS_USER}/.ssh/authorized_keys
fi

chown -R ${SSH_KEYS_USER}:${SSH_KEYS_USER} /home/${SSH_KEYS_USER}


function make_systemd_path() {
cat >"/etc/systemd/system/ssh_auth.path" <<EOL
[Unit]
Description=Checking authrized_keys for changes

[Path]
PathModified=/home/${SSH_KEYS_USER}/.ssh/authorized_keys

[Install]
WantedBy=multi-user.target
EOL
}


function make_systemd_service() {
cat >"/etc/systemd/system/ssh_auth.service" <<EOL
[Unit]
Description=Check changes on authorized_keys and modify user accounts if needed

[Service]
ExecStart=/usr/bin/ssh_auth_users

[Install]
WantedBy=multi-user.target
EOL
}


function set_rrsync() {
if [[ ! -f /usr/bin/rrsync ]] ;then
  rrsync_doc_folder="$(ls /usr/share/doc/ | grep rsync)"
  if [[ ! "${rrsync_doc_folder}" ]]; then
    echo "could not find rrsync path :( exiting"
    exit 1
  fi
  rrsync_doc_folder=/usr/share/doc/${rrsync_doc_folder}
  #Centos:
  if [[ -f "${rrsync_doc_folder}/support/rrsync" ]]; then
      cp "${rrsync_doc_folder}/support/rrsync" /usr/bin/rrsync
      chmod 755 /usr/bin/rrsync
  #Ubuntu:
  elif [[ -f "${rrsync_doc_folder}/scripts/rrsync.gz" ]]; then
      cp "${rrsync_doc_folder}/scripts/rrsync.gz"  /usr/bin/
      gzip -d /usr/bin/rrsync.gz
      chmod 755 /usr/bin/rrsync
  else echo "could not find rrsync :("
  fi
fi
}


function setup_ubuntu() {
  if [[ ! -x "$(command -v rsync)" ]]; then
    apt install curl -y
  fi
  if [[ ! -x "$(command -v perl)" ]]; then
    apt install perl -y
  fi
  ufw allow "${SSH_PORT}" >/dev/null
  ufw_status="$(sudo ufw status | cut -d ' ' -f2)"
  if [[ "${ufw_status}" == "inactive" ]]; then
    echo 'y' | ufw enable
  fi
}


function setup_centos() {
  if [[ ! $(rpm -qa | grep "epel-release") ]]; then
    yum install epel-release -y
  fi
  if [[ ! -x "$(command -v semanage)" ]]; then
    yum install policycoreutils-python -y
  fi
  if [[ ! -x "$(command -v rsync)" ]]; then
    yum install -y rsync
  fi
  if [[ ! $(semanage port -l | grep ${SSH_PORT}) ]]; then
    semanage port -a -t ssh_port_t -p tcp ${SSH_PORT}
  fi
  if [[ ! -x "$(command -v perl)" ]]; then
    yum install -y perl
  fi
  if [[ ! "$(systemctl is-active firewalld)" == "active" ]]; then
    systemctl start firewalld
  fi
  if [[ ! "$(systemctl is-enabled firewalld)" == "enabled" ]]; then
    systemctl enable firewalld
  fi
  semanage permissive -a rsync_t
  firewall-cmd --permanent --zone=public --add-port=${SSH_PORT}/tcp >/dev/null
  firewall-cmd --reload
}

function check_distro() {
  if [[ -f /etc/os-release ]]; then
  . /etc/os-release
  DISTRO="${ID}"
  if [[  "${DISTRO}" == "ubuntu" || "${DISTRO}" == "centos" ]]; then
      if [[ "${DISTRO}" == "ubuntu" ]]; then
        setup_ubuntu
      else
        setup_centos
      fi
      set_rrsync
  else echo "Only tested on ubuntu and centos... exiting"
  exit 1
  fi
  else
    echo "Could not determine distro ... exiting"
    exit 1
  fi
}

function make_users_script() {
  cat >"/usr/bin/ssh_auth_users" <<EOL
#!/bin/bash
while IFS='' read -r line || [[ -n "\${line}" ]]; do
  echo "\${line}" > "/tmp/tmp.pub"
  if [[ ! "\$(ssh-keygen -E md5 -lf /tmp/tmp.pub | grep MD5)" ]]; then
    echo "Invalid key: \${line}" >  /var/log/ssh_auth_users.log
  elif [[ ! "\$(cat /home/${SSH_KEYS_USER}/.ssh/id_rsa.pub | grep "\${line}")" ]]; then
    user_id="\$(ssh-keygen -lf /tmp/tmp.pub -E md5 | awk '{print \$2}' | sed 's/://g' | sed 's/MD5//g')"
    if ! id "\${user_id}" >/dev/null 2>&1; then
      useradd -d /home/\${user_id} \${user_id}
    fi
    mkdir -p /home/\${user_id}/.ssh
    mkdir -p /home/\${user_id}/backups
    echo "command=\"/usr/bin/rrsync /home/\${user_id}/backups\",no-agent-forwarding,no-port-forwarding,no-pty,no-user-rc,no-X11-forwarding \${line}" >  /home/\${user_id}/.ssh/authorized_keys
    chown -R \${user_id}:\${user_id} /home/\${user_id}
  fi
done < /home/${SSH_KEYS_USER}/.ssh/authorized_keys
EOL
  chmod +x "/usr/bin/ssh_auth_users"
  chmod +x "/usr/bin/ssh_auth_users"
}

function set_sshd_conf() {
  sed -i 's|.*Port .*|Port '${SSH_PORT}'|' /etc/ssh/sshd_config;
  sed -i 's|.*PasswordAuthentication .*|PasswordAuthentication no|' /etc/ssh/sshd_config;
}

check_distro
make_users_script
make_systemd_path
make_systemd_service
systemctl daemon-reload
systemctl enable ssh_auth.path
systemctl enable ssh_auth.service
systemctl start ssh_auth.path
systemctl start ssh_auth.service
set_sshd_conf
systemctl restart sshd
# TODO: quota setup for each user
# http://tldp.org/HOWTO/Quota-3.html#ss3.5
