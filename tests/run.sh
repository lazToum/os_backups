#!/bin/bash

set -e

HERE=$(cd -P -- "$(dirname -- "$0")" && pwd -P)

cd "${HERE}"
if [[ ! -f "${HERE}/.env" ]];then
  echo "Please include a .env file containing the SHARED_SECRET and OAUTH_VALIDATION_URL variables : SHARED_SECRET=......"
  exit 1
fi
. "${HERE}/.env"


function echo_usage() {
  echo  -n "Usage: ${0} servers|clients|all
      Start servers, clients or both and run tests
"
}


function test_clients() {
  echo "Creating ubuntu/bionic64 client with no database and testing invalid proxy ip"
  echo "Crating a new ubuntu/bionic64 instance..."
  "${HERE}/client.sh" make ubuntu bionic 1>/dev/null
#  ./client.sh make ubuntu bionic && ./client.sh install mariadb && ./client.sh import mariadb
#  ./client.sh make ubuntu bionic && ./client.sh install mysql && ./client.sh import mysql
#  ./client.sh make ubuntu bionic && ./client.sh install mongodb && ./client.sh import mongodb

  echo "Testing no files, no database, no rsync and invalid proxy post"
  echo "Testing no files, no database, no rsync but valid proxy post"
  echo "Testing no files, no database, rsync after valid proxy post"
  echo "Crating a new ubuntu/xenial64 instance..."
  "${HERE}/client.sh" make ubuntu xenial 1>/dev/null

  echo "Creating centos/7 client and testing no rsync and proxy post"
  echo "Crating a new centos/7 instance..."
  "${HERE}/client.sh" make centos 1>/dev/null

  echo "Creating centos/7 client and testing mongodb"
  echo "Installing mongodb on the client..."

  echo "Creating centos/7 client and testing postgresql"
  echo "Creating centos/7 client and testing mysql"
  echo "Creating centos/7 client and testing mariadb"
}

function test_servers() {
  if [[ ! -x "${HERE}/servers.sh" ]]; then
    chmod +x "${HERE}/servers.sh"
  fi
  echo "This will take a lot of time...."
  "${HERE}/servers.sh" destroy 1>/dev/null
  sleep 5
  echo
  echo "Creating centos/7 backup server and ubuntu/xenial64 keys proxy"
  echo
  sed -i "s/BACKUP_SERVER_OS.*/BACKUP_SERVER_OS=\"centos\"/" "${HERE}/.env"
  sed -i "s/KEYS_PROXY_SERVER_OS.*/KEYS_PROXY_SERVER_OS=\"ubuntu\"/" "${HERE}/.env"
  sed -i "s/UBUNTU_SERVER_FLAVOR.*/UBUNTU_SERVER_FLAVOR=\"xenial64\"/" "${HERE}/.env"
  "${HERE}/servers.sh" make 1>/dev/null
  echo
  echo "Servers ready... continuing"
  sleep 5
  echo
  echo "creating centos/7 backup server and ubuntu/bionic64 keys proxy"
  sed -i "s/BACKUP_SERVER_OS.*/BACKUP_SERVER_OS=\"centos\"/" "${HERE}/.env"
  sed -i "s/KEYS_PROXY_SERVER_OS.*/KEYS_PROXY_SERVER_OS=\"ubuntu\"/" "${HERE}/.env"
  sed -i "s/UBUNTU_SERVER_FLAVOR.*/UBUNTU_SERVER_FLAVOR=\"bionic64\"/" "${HERE}/.env"
  "${HERE}/servers.sh" make 1>/dev/null
  echo
  echo "Servers ready... continuing"
  sleep 5
  echo
  echo "creating ubuntu/bionic64 backup server and centos/7 keys proxy"
  sed -i "s/BACKUP_SERVER_OS.*/BACKUP_SERVER_OS=\"ubuntu\"/" "${HERE}/.env"
  sed -i "s/KEYS_PROXY_SERVER_OS.*/KEYS_PROXY_SERVER_OS=\"centos\"/" "${HERE}/.env"
  sed -i "s/UBUNTU_SERVER_FLAVOR.*/UBUNTU_SERVER_FLAVOR=\"bionic64\"/" "${HERE}/.env"
  "${HERE}/servers.sh" make 1>/dev/null
  echo
  echo "Servers ready... continuing"
  sleep 5
  echo
  echo "creating ubuntu/xenial64 backup server and centos/7 keys proxy"
  sed -i "s/BACKUP_SERVER_OS.*/BACKUP_SERVER_OS=\"ubuntu\"/" "${HERE}/.env"
  sed -i "s/KEYS_PROXY_SERVER_OS.*/KEYS_PROXY_SERVER_OS=\"centos\"/" "${HERE}/.env"
  sed -i "s/UBUNTU_SERVER_FLAVOR.*/UBUNTU_SERVER_FLAVOR=\"xenial64\"/" "${HERE}/.env"
  "${HERE}/servers.sh" make 1>/dev/null
  echo
  echo "Servers ready... continuing"
  sleep 5
  echo
  echo "creating ubuntu/bionic64 backup server and ubuntu/bionic64 keys proxy"
  sed -i "s/BACKUP_SERVER_OS.*/BACKUP_SERVER_OS=\"ubuntu\"/" "${HERE}/.env"
  sed -i "s/KEYS_PROXY_SERVER_OS.*/KEYS_PROXY_SERVER_OS=\"ubuntu\"/" "${HERE}/.env"
  sed -i "s/UBUNTU_SERVER_FLAVOR.*/UBUNTU_SERVER_FLAVOR=\"bionic64\"/" "${HERE}/.env"
  "${HERE}/servers.sh" make 1>/dev/null
  echo
  echo "Servers ready... continuing"
  sleep 5
  echo
  echo "creating ubuntu/xenial64 backup server and ubuntu/xenial64 keys proxy"
  sed -i "s/BACKUP_SERVER_OS.*/BACKUP_SERVER_OS=\"ubuntu\"/" "${HERE}/.env"
  sed -i "s/KEYS_PROXY_SERVER_OS.*/KEYS_PROXY_SERVER_OS=\"ubuntu\"/" "${HERE}/.env"
  sed -i "s/UBUNTU_SERVER_FLAVOR.*/UBUNTU_SERVER_FLAVOR=\"xenial64\"/" "${HERE}/.env"
  "${HERE}/servers.sh" make 1>/dev/null
  echo
  echo "Servers ready... continuing"
  sleep 5
  echo
  echo "creating centos/7 backup server and centos/7 keys proxy"
  sed -i "s/BACKUP_SERVER_OS.*/BACKUP_SERVER_OS=\"centos\"/" "${HERE}/.env"
  sed -i "s/KEYS_PROXY_SERVER_OS.*/KEYS_PROXY_SERVER_OS=\"centos\"/" "${HERE}/.env"
  "${HERE}/servers.sh" make 1>/dev/null
  echo
  echo "Servers ready... continuing"
  sleep 5
  echo
  echo "creating ubuntu/bionic64 backup server and ubuntu/bionic64 keys proxy (same machine/ip)"
  sed -i "s/BACKUP_SERVER_OS.*/BACKUP_SERVER_OS=\"ubuntu\"/" "${HERE}/.env"
  sed -i "s/KEYS_PROXY_SERVER_OS.*/KEYS_PROXY_SERVER_OS=\"ubuntu\"/" "${HERE}/.env"
  sed -i "s/BACKUP_SERVER_IP.*/BACKUP_SERVER_IP=\"192.168.56.101\"/" "${HERE}/.env"
  sed -i "s/KEYS_PROXY_SERVER_IP.*/KEYS_PROXY_SERVER_IP=\"192.168.56.101\"/" "${HERE}/.env"
  sed -i "s/UBUNTU_SERVER_FLAVOR.*/UBUNTU_SERVER_FLAVOR=\"bionic64\"/" "${HERE}/.env"
  "${HERE}/servers.sh" make 1>/dev/null
  echo
  echo "Servers ready... continuing"
  sleep 5
  echo
  echo "creating centos/7 backup server and centos/7 keys proxy (same machine/ip)"
  sed -i "s/BACKUP_SERVER_OS.*/BACKUP_SERVER_OS=\"centos\"/" "${HERE}/.env"
  sed -i "s/KEYS_PROXY_SERVER_OS.*/KEYS_PROXY_SERVER_OS=\"centos\"/" "${HERE}/.env"
  sed -i "s/BACKUP_SERVER_IP.*/BACKUP_SERVER_IP=\"192.168.56.101\"/" "${HERE}/.env"
  sed -i "s/KEYS_PROXY_SERVER_IP.*/KEYS_PROXY_SERVER_IP=\"192.168.56.101\"/" "${HERE}/.env"
  "${HERE}/servers.sh" make 1>/dev/null
  echo
  echo "Servers ready... continuing"
  sleep 5
  echo
  echo "creating ubuntu/xenial64 backup server and ubuntu/xenial64 keys proxy (same machine/ip)"
  sed -i "s/BACKUP_SERVER_OS.*/BACKUP_SERVER_OS=\"ubuntu\"/" "${HERE}/.env"
  sed -i "s/KEYS_PROXY_SERVER_OS.*/KEYS_PROXY_SERVER_OS=\"ubuntu\"/" "${HERE}/.env"
  sed -i "s/BACKUP_SERVER_IP.*/BACKUP_SERVER_IP=\"192.168.56.101\"/" "${HERE}/.env"
  sed -i "s/KEYS_PROXY_SERVER_IP.*/KEYS_PROXY_SERVER_IP=\"192.168.56.101\"/" "${HERE}/.env"
  sed -i "s/UBUNTU_SERVER_FLAVOR.*/UBUNTU_SERVER_FLAVOR=\"xenial64\"/" "${HERE}/.env"
  "${HERE}/servers.sh" make 1>/dev/null
  echo
  echo "All servers created successfully..."
  sleep 5
  "${HERE}/servers.sh" destroy 1>/dev/null
}

function test_all() {
  test_servers
  sleep 5
  echo "creating ubuntu/xenial64 backup server and ubuntu/xenial64 keys proxy (same machine/ip)"
  "${HERE}/servers.sh" make 1>/dev/null
  echo "Servers ready... continuing"
  test_clients
}

case "$1" in
  servers)
  test_servers
    ;;
  clients)
    test_clients
    ;;
  all)
    test_all
    ;;
  *)
    echo_usage
    ;;
esac

