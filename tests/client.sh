#!/bin/bash

set -e

if [[ ! -x "$(command -v vagrant)" ]];then
  echo "Command vagrant not found. Is it installed?"
  exit 1
fi
if [[ ! -x "$(command -v virtualbox)" ]];then
  echo "Virtualbox not found. Is it installed?"
  exit 1
fi
HERE=$(cd -P -- "$(dirname -- "$0")" && pwd -P)

cd "${HERE}"
if [[ ! -f "${HERE}/.env" ]];then
  echo "Please include a .env file containing SHARED_SSH_USER, SHARED_SECRET, OAUTH_VALIDATION_URL ... variables"
  exit 1
fi


. "${HERE}/.env"
SHARED_SECRET=${SHARED_SECRET:-}
OAUTH_VALIDATION_URL=${OAUTH_VALIDATION_URL:-}
OAUTH_TOKEN_HOST=${OAUTH_TOKEN_HOST:-}
OAUTH_CLIENT_ID=${OAUTH_CLIENT_ID:-}
OAUTH_CLIENT_SECRET=${OAUTH_CLIENT_SECRET:-}
KEYS_PROXY_SERVER_IP=${KEYS_PROXY_SERVER_IP:-}
BACKUP_SERVER_IP=${BACKUP_SERVER_IP:-}
CLIENT_IP=${CLIENT_IP:-"192.168.56.105"}
CLIENT_OS=${CLIENT_OS:-"centos"}
SSH_PORT=${SSH_PORT:-2244}
NGINX_PORT=${NGINX_PORT:-7678}
CLIENT_UBUNTU_FLAVOR=${CLIENT_UBUNTU_FLAVOR:-"bionic64"}
DB_ADMIN_ROOT_PASSWD=${DB_ADMIN_ROOT_PASSWD:-"password"}
DB_SERVICE_USER=${DB_SERVICE_USER:-"testuser"}
DB_SERVICE_PASSWORD=${DB_SERVICE_PASSWORD:-"testpassword"}
MONGO_DUMP_DB_NAME=${MONGO_DUMP_DB_NAME:-"SampleCollection"}
MYSQL_DUMP_DB_NAME=${MYSQL_DUMP_DB_NAME:-"sakila"}
POSTGRESQL_DUMP_DB_NAME=${POSTGRESQL_DUMP_DB_NAME:-"dvdrental"}
BACKUP_FILES=${BACKUP_FILES:-0}
BACKUP_DBS=${BACKUP_DBS:-0}
BACKUP_ALL_DBS=${BACKUP_ALL_DBS:-0}
BACKUP_MONGO=${BACKUP_MONGO:-0}
BACKUP_MONGO_AUTH=${BACKUP_MONGO_AUTH:-1}
BACKUP_MYSQL=${BACKUP_MYSQL:-0}
BACKUP_MYSQL_AUTH=${BACKUP_MYSQL_AUTH:-1}
BACKUP_MARIADB=${BACKUP_MARIADB:-0}
BACKUP_MARIADB_AUTH=${BACKUP_MARIADB_AUTH:-1}
BACKUP_POSTGRESQL=${BACKUP_POSTGRESQL:-0}
BACKUP_POSTGRESQL_AUTH=${BACKUP_POSTGRESQL_AUTH:-1}
client_vm_name=backup_client_vm

if [[ -d "${HERE}/client" ]]; then
  mkdir -p "${HERE}/client"
fi

mkdir -p "${HERE}/client/backup"

function echo_usage() {
  echo  -n "Usage:
            ${0} make OPTION1 OPTION2
            ${0} start|ssh|stop|destroy
            ${0} install|import|backup|restore OPTION

            make OPTION1: centos|ubuntu
            make OPTION2 (if OPTION1=ubuntu): xenial bionic
            install|import OPTION: mongodb|postgresql|mysql|mariadb
            backup|restore OPTION: files|databases|all

"
}

function set_files() {
  cp ${HERE}/../client/backup.sh "${HERE}/client/backup/backup.sh"
  cp ${HERE}/../client/restore.sh "${HERE}/client/backup/restore.sh"
}

function _client_run_script() {
script_path="${1}"
if [[ -f "${script_path}" ]]; then
  cd "${HERE}/client"
  path_in_vm="$(echo "${script_path}" | sed "s|${HERE}||g" | sed "s|client|vagrant|")"
  if [[ $# -eq 2 ]]; then
    vagrant rsync && vagrant ssh "${client_vm_name}" -c "sudo chmod +x ${path_in_vm} && sudo ${path_in_vm} ${2}"
  else
    vagrant rsync && vagrant ssh "${client_vm_name}" -c "sudo chmod +x ${path_in_vm} && sudo ${path_in_vm}"
   fi
fi
cd "${HERE}"
}

function _clear() {
if [[ -f "${HERE}/client/Vagrantfile" ]]; then
  rm "${HERE}/client/Vagrantfile"
fi
if [[ -d "${HERE}/client/files_backup" ]]; then
  rm -rf "${HERE}/client/files_backup"
fi
if ls ${HERE}/client/backup/* 1>/dev/null 2>&1; then
    rm -rf ${HERE}/client/backup/*
fi
if ls ${HERE}/client/.vagrant/machines/* 1>/dev/null 2>&1; then
  rm -rf ${HERE}/client/.vagrant/machines/*
fi

}

function make_client_vm() {

box_os="ubuntu/${CLIENT_UBUNTU_FLAVOR}"
if [[ "${CLIENT_OS}" == "centos" ]]; then
  box_os="centos/7"
fi
  cat >"${HERE}/client/Vagrantfile" <<EOL
Vagrant.configure("2") do |config|
  config.vm.define "backup_client_vm" do |backup_client_vm|
    backup_client_vm.vm.box = "${box_os}"
    backup_client_vm.vm.network :private_network, ip: "${CLIENT_IP}"
    backup_client_vm.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--memory", 1024]
      v.customize ["modifyvm", :id, "--name", "${client_vm_name}"]
    end
  end
end
EOL
cd "${HERE}/client"
vagrant up
cd "${HERE}"
set_files
sleep 5
}

function destroy_client_vm() {
  if [[ -f "${HERE}/client/Vagrantfile" ]]; then
    cd "${HERE}/client"
    vagrant destroy -f
    cd "${HERE}"
  fi
  _clear
}

function start_client_vm() {
  if [[ -f "${HERE}/client/Vagrantfile" ]]; then
    cd "${HERE}/client"
    if [[ ! -z "$(vagrant status "${client_vm_name}" | awk '{print $2}' | grep 'poweroff')" || "$(vagrant status "${client_vm_name}" | awk '{print $2}' | grep 'poweroff')" ]]; then
      vagrant up --no-provision "${client_vm_name}"
    fi
fi
cd "${HERE}"
}

function stop_client_vm() {
  if [[ -f "${HERE}/client/Vagrantfile" ]]; then
    cd "${HERE}/client"
    if [[ ! -z "$(vagrant status "${client_vm_name}" | awk '{print $2}' | grep 'running')" ]]; then
      vagrant ssh "${client_vm_name}" -c "sudo poweroff" || echo
    fi
  fi
cd "${HERE}"
}

function client_vm_status() {
   if [[ -f "${HERE}/client/Vagrantfile" ]]; then
    cd "${HERE}/client"
    vagrant status | grep "${client_vm_name}"
  else
    echo "No Vagrantfile in folder ${HERE}/client... No vms or destroyed"
  fi;
  cd "${HERE}"
}

function ssh_client_vm() {
  if [[ -f "${HERE}/client/Vagrantfile" ]]; then
    cd "${HERE}/client"
    if [[ ! -z "$(vagrant status "${client_vm_name}" | awk '{print $2}' | grep 'running')" ]]; then
      vagrant ssh "${client_vm_name}"
    else
      cd "${HERE}"
    fi
  else
      cd "${HERE}"
  fi
}

function install_postgresql_centos() {
if [[ -f "${HERE}/client/backup/script.sh" ]]; then
  rm "${HERE}/client/backup/script.sh"
fi
cat >"${HERE}/client/backup/script.sh" <<EOL
#!/bin/bash
sudo yum install postgresql-server postgresql-contrib -y
sudo postgresql-setup initdb
sudo sed -ri 's|ident|md5|' /var/lib/pgsql/data/pg_hba.conf
sudo systemctl restart postgresql
EOL
_client_run_script "${HERE}/client/backup/script.sh"
rm "${HERE}/client/backup/script.sh"
}

function install_postgresql_ubuntu() {
if [[ -f "${HERE}/client/backup/script.sh" ]]; then
  rm "${HERE}/client/backup/script.sh"
fi
cat >"${HERE}/client/backup/script.sh" <<EOL
#!/bin/bash
sudo apt update && sudo apt install postgresql postgresql-contrib -y
pg_version="\$(ls /etc/postgresql)"
sudo sed -ri 's|ident|md5|' /etc/postgresql/\${pg_version}/main/pg_hba.conf
sudo systemctl restart postgresql
EOL
_client_run_script "${HERE}/client/backup/script.sh"
rm "${HERE}/client/backup/script.sh"
}

function install_mariadb_ubuntu() {
ubuntu_flavor="$( echo ${CLIENT_UBUNTU_FLAVOR} | sed 's|64||')"
cat >"${HERE}/client/backup/script.sh" <<EOL
#!/bin/bash
sudo apt-get install software-properties-common
sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
sudo add-apt-repository 'deb [arch=amd64] http://ftp.cc.uoc.gr/mirrors/mariadb/repo/10.3/ubuntu ${ubuntu_flavor} main'
sudo apt update
sudo export DEBIAN_FRONTEND=noninteractive
sudo debconf-set-selections <<< "mariadb-server-10.3 mysql-server/root_password password ${DB_ADMIN_ROOT_PASSWD}"
sudo debconf-set-selections <<< "mariadb-server-10.3 mysql-server/root_password_again password ${DB_ADMIN_ROOT_PASSWD}"
sudo apt install mariadb-server -y
sudo systemctl restart mariadb
EOL
_client_run_script "${HERE}/client/backup/script.sh"
rm "${HERE}/client/backup/script.sh"
}

function install_mariadb_centos() {
cat >"${HERE}/client/backup/script.sh" <<EOL
#!/bin/bash
curl -sS https://downloads.mariadb.com/MariaDB/mariadb_repo_setup | sudo bash
sudo yum install MariaDB-server MariaDB-client -y
sudo systemctl start mariadb
mysql -u root -e"SET PASSWORD FOR 'root'@'localhost' = PASSWORD('${DB_ADMIN_ROOT_PASSWD}');FLUSH PRIVILEGES;"
sudo systemctl restart mariadb
EOL
_client_run_script "${HERE}/client/backup/script.sh"
rm "${HERE}/client/backup/script.sh"
}

function install_mysql_ubuntu() {
cat >"${HERE}/client/backup/script.sh" <<EOL
#!/bin/bash
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password ${DB_ADMIN_ROOT_PASSWD}"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password ${DB_ADMIN_ROOT_PASSWD}"
sudo apt install -y mysql-server
sudo systemctl restart mysql
EOL
_client_run_script "${HERE}/client/backup/script.sh"
rm "${HERE}/client/backup/script.sh"
}

function install_mysql_centos() {
cat >"${HERE}/client/backup/script.sh" <<EOL
#!/bin/bash
  if [[ ! -x "$(command -v wget)" ]]; then
    sudo yum install wget -y
  fi
  wget https://dev.mysql.com/get/mysql80-community-release-el7-2.noarch.rpm
  sudo rpm -ivh mysql80-community-release-el7-2.noarch.rpm
  sudo rm  mysql80-community-release-el7-2.noarch.rpm
  sudo yum update -y
  sudo yum install mysql-server -y
  sudo systemctl start mysqld
  tmp_pass="\$(sudo cat /var/log/mysqld.log | grep 'root@localhost' |  awk '{print \$NF}')"
  mysql --connect-expired-password -u root -p"\${tmp_pass}" -e "ALTER USER 'root'@'localhost' IDENTIFIED BY '\${tmp_pass}';"
  if [[ ! \$(cat /etc/my.cnf | grep validate) ]]; then
    cp /etc/my.cnf /tmp/_my.cnf
    echo "validate_password.policy=LOW" >> /tmp/_my.cnf
    sudo mv /tmp/_my.cnf /etc/my.cnf
  fi
  sudo systemctl restart mysqld
  mysql -u root -p"\${tmp_pass}" -e "ALTER USER 'root'@'localhost' IDENTIFIED BY '${DB_ADMIN_ROOT_PASSWD}';"
  sudo systemctl restart mysqld
EOL
_client_run_script "${HERE}/client/backup/script.sh"
rm "${HERE}/client/backup/script.sh"
}


function start_mongo() {
  cp "${HERE}/client/run_mongo.sh" "${HERE}/client/backup/script.sh"
  chmod +x "${HERE}/client/backup/script.sh"
  cp "${HERE}/.env" "${HERE}/client/backup/.mongo.env"
  _client_run_script "${HERE}/client/backup/script.sh"
  rm "${HERE}/client/backup/script.sh"
  rm "${HERE}/client/backup/.mongo.env"
}


function install_mongo_ubuntu() {
if [[ -f "${HERE}/client/backup/script.sh" ]]; then
  rm "${HERE}/client/backup/script.sh"
fi
ubuntu_flavor="$( echo ${CLIENT_UBUNTU_FLAVOR} | sed 's|64||')"
cat >"${HERE}/client/backup/script.sh" <<EOL
#!/bin/bash
echo "adding mongodb repository"
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu ${ubuntu_flavor}/mongodb-org/4.0 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-4.0.list
echo Updating and installing mongodb ...
sudo apt update && sudo apt install -y mongodb-org
sudo systemctl stop mongod
sudo systemctl disable mongod
EOL
_client_run_script "${HERE}/client/backup/script.sh"
rm "${HERE}/client/backup/script.sh"
start_mongo
}

function install_mongo_centos() {
if [[ -f "${HERE}/client/backup/script.sh" ]]; then
  rm "${HERE}/client/backup/script.sh"
fi
cat >"${HERE}/client/backup/script.sh" <<EOL
#!/bin/bash
  cat >"/home/vagrant/mongodb-org-4.0.repo" <<LOE
[mongodb-org-4.0]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/7/mongodb-org/4.0/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-4.0.asc
LOE
sudo mv "/home/vagrant/mongodb-org-4.0.repo" /etc/yum.repos.d/mongodb-org-4.0.repo
sudo yum install -y mongodb-org
sudo systemctl stop mongod
sudo systemctl disable mongod
EOL
_client_run_script "${HERE}/client/backup/script.sh"
rm "${HERE}/client/backup/script.sh"
start_mongo
}

function client_install() {
  case "$1" in
  "mongodb")
    if [[ "${CLIENT_OS}" == "ubuntu" ]]; then
      install_mongo_ubuntu
    elif [[ "${CLIENT_OS}" == "centos" ]]; then
      install_mongo_centos
    fi
    ;;
  "postgresql")
    if [[ "${CLIENT_OS}" == "ubuntu" ]]; then
      install_postgresql_ubuntu
    elif [[ "${CLIENT_OS}" == "centos" ]]; then
      install_postgresql_centos
    fi
    ;;
  "mysql")
    if [[ "${CLIENT_OS}" == "ubuntu" ]]; then
      install_mysql_ubuntu
    elif [[ "${CLIENT_OS}" == "centos" ]]; then
      install_mysql_centos
    fi
    ;;
  "mariadb")
    if [[ "${CLIENT_OS}" == "ubuntu" ]]; then
      install_mariadb_ubuntu
    elif [[ "${CLIENT_OS}" == "centos" ]]; then
      install_mariadb_centos
    fi
    ;;
   *)
    echo_usage;;
  esac
}

function import_mongo_data() {
  if [[ ! -d "${HERE}/client/db_samples/mongodb/dump" ]]; then
    echo "dump folder: ${HERE}/client/db_samples/mongodb/dump not found..."
    exit 1
  fi
start_mongo
if [[ ${BACKUP_MONGO_AUTH} -eq 0 ]];then
  cat >"${HERE}/client/backup/script.sh" <<EOL
#!/bin/bash
sudo mongorestore --username="admin" --password="${DB_ADMIN_ROOT_PASSWD}" --authenticationDatabase="admin" --drop "/vagrant/db_samples/mongodb/dump"
EOL
else
  cat >"${HERE}/client/backup/script.sh" <<EOL
#!/bin/bash
sudo mongorestore --drop "/vagrant/db_samples/mongodb/dump"
EOL
fi
_client_run_script "${HERE}/client/backup/script.sh"
rm "${HERE}/client/backup/script.sh"

}

function import_postgresql_data() {
  if [[ ! -f "${HERE}/client/db_samples/postgresql/dump.tar" ]]; then
    echo "dump file: ${HERE}/client/db_samples/postgresql/dump.tar not found..."
    exit 1
  fi
  cat >"${HERE}/client/backup/script.sh" <<EOL
#!/bin/bash
  sudo systemctl start postgresql
  sudo -u postgres -H -- psql -c "DROP DATABASE IF EXISTS ${POSTGRESQL_DUMP_DB_NAME};"
  sudo -u postgres -H -- psql -c "CREATE DATABASE ${POSTGRESQL_DUMP_DB_NAME};"
  sudo -u postgres -H pg_restore -d ${POSTGRESQL_DUMP_DB_NAME} /vagrant/db_samples/postgresql/dump.tar
EOL
_client_run_script "${HERE}/client/backup/script.sh"
rm "${HERE}/client/backup/script.sh"
}

function import_mysql_data() {
  my_maria="$1"
  if [[ ! -d "${HERE}/client/db_samples/mysql/dump" ]]; then
    echo "dump folder: ${HERE}/client/db_samples/mysql/dump not found..."
    exit 1
  fi
  if ls ${HERE}/client/db_samples/mysql/dump/*.sql 1>/dev/null 2>&1; then
    sqls=($(ls ${HERE}/client/db_samples/mysql/dump/*.sql))
    ordered_sqls=()
    if [[ "$(ls ${HERE}/client/db_samples/mysql/dump/*.sql | grep schema)" ]];then
      schema_sql="$(ls ${HERE}/client/db_samples/mysql/dump/*.sql | grep schema)"
      ordered_sqls+=("${schema_sql}")
      for sql in ${sqls[@]}; do
        if [[ "${sql}" != "${schema_sql}" ]]; then
          ordered_sqls+=("$(echo ${sql} | sed 's|${HERE}/client|/vagrant|')")
        fi
      done
    else
      for sql in ${sqls[@]}; do
        ordered_sqls+=("${sql}")
      done
    fi
    _str=""
    for sql in ${ordered_sqls[@]}; do
    path_in_vm="$(echo ${sql} | sed "s|${HERE}/client|/vagrant|")"
      _str="${_str}SOURCE ${path_in_vm};"
    done
    systemd_name="${my_maria}"
    if [[ "${my_maria}" == "mysql" && "${CLIENT_OS}" == "centos" ]]; then
      systemd_name="mysqld"
    fi
      cat >"${HERE}/client/backup/script.sh" <<EOL
#!/bin/bash
sudo systemctl start ${systemd_name}
mysql -u root -p"${DB_ADMIN_ROOT_PASSWD}" -e "${_str}"
EOL
_client_run_script "${HERE}/client/backup/script.sh"
rm "${HERE}/client/backup/script.sh"
   else
    echo "No .sql files found in ${HERE}/client/db_samples/mysql/dump/"
  fi
}

function client_import() {
case "$1" in
  "mongodb")
    import_mongo_data
  ;;
  "postgresql")
    import_postgresql_data
  ;;
  "mysql"|"mariadb")
    import_mysql_data "${1}"
  ;;
  *)
    echo_usage;;
  esac
}


function add_mongo_user() {
      cat >"${HERE}/client/backup/script.sh" <<EOL
#!/bin/bash
mongo "${MONGO_DUMP_DB_NAME}" --quiet --eval "db.dropUser('${DB_SERVICE_USER}')" > /dev/null
mongo "${MONGO_DUMP_DB_NAME}" --eval "db.createUser({user: '${DB_SERVICE_USER}', pwd: '${DB_SERVICE_PASSWORD}', roles:[{role:'dbAdmin',db:'"${MONGO_DUMP_DB_NAME}"'}, {role:'readWrite',db:'"${MONGO_DUMP_DB_NAME}"'}]});"
EOL
}

function add_postgres_user() {
      cat >"${HERE}/client/backup/script.sh" <<EOL
#!/bin/bash
sudo systemctl restart postgresql
sudo -u postgres -H -- psql -c "DROP ROLE IF EXISTS ${DB_SERVICE_USER}";
sudo -u postgres -H -- psql -c "CREATE USER ${DB_SERVICE_USER} with encrypted password '${DB_SERVICE_PASSWORD}';"
sudo -u postgres -H -- psql -c "GRANT ALL PRIVILEGES ON DATABASE ${POSTGRESQL_DUMP_DB_NAME} TO ${DB_SERVICE_USER} WITH GRANT OPTION;"
sudo -u postgres -H -- psql -c "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO ${DB_SERVICE_USER};"
sudo -u postgres -H -- psql -c "GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO ${DB_SERVICE_USER};"
sudo systemctl restart postgresql
EOL
_client_run_script "${HERE}/client/backup/script.sh"
rm "${HERE}/client/backup/script.sh"
}


function add_mysql_mariadb_user() {
  echo
  #mysql --user=root --password=root --execute="CREATE USER 'admin'@'%' IDENTIFIED BY 'admin';" \
#mysql --user=root --password=root --execute="CREATE DATABASE testdb CHARACTER SET utf8 COLLATE utf8_bin;" \
#mysql --user=root --password=root --execute="GRANT ALL PRIVILEGES on testdb.* TO 'admin'@'%' IDENTIFIED BY 'admin';" \
#mysql --user=root --password=root --execute="FLUSH PRIVILEGES;"
}


function make_backup_json() {
. "${HERE}/.env"
  cat >"${HERE}/client/backup/backup.json" <<EOL
{
  "TIMER_CALENDAR": "Sun *-*-* 00:00:00",
  "BACKUPS_FOLDER": "/vagrant/backups",
  "BACKUPS_TO_KEEP": 3,
  "PARTNER": "UWA",
  "BACKUP_LOCAL_USER": "backer",
  "KEYS_SERVER_HTTP_HOST":"http://${KEYS_PROXY_SERVER_IP}",
  "KEYS_SERVER_HTTP_PORT":${NGINX_PORT},
  "BACKUP_REMOTE_SSH_HOST":"${BACKUP_SERVER_IP}",
  "BACKUP_REMOTE_SSH_PORT":${SSH_PORT},
  "OAUTH_TOKEN_HOST":"${OAUTH_TOKEN_HOST}",
  "OAUTH_CLIENT_ID" : "${OAUTH_CLIENT_ID}",
  "OAUTH_CLIENT_SECRET" : "${OAUTH_CLIENT_SECRET}",
  "SHARED_SECRET": "${SHARED_SECRET}",
EOL
if [[ "${BACKUP_FILES}" -ne 0 ]]; then
cat >>"${HERE}/client/backup/backup.json" <<EOL
"FILES_BACKUP": {
		"ENABLED": false
},
EOL
else
mkdir -p ${HERE}/client/files_backup
if ls ${HERE}/client/files_backup/* 1>/dev/null 2>&1; then
    rm -rf ${HERE}/client/files_backup/*
fi
echo "text" > ${HERE}/client/files_backup/file.txt
mkdir -p ${HERE}/client/files_backup/cache
echo "cache" > ${HERE}/client/files_backup/cache/cache.txt
mkdir -p ${HERE}/client/files_backup/.git
echo "ignore" > ${HERE}/client/files_backup/.git/ignore.txt
cat >>"${HERE}/client/backup/backup.json" <<EOL
  "FILES_BACKUP": {
    "ENABLED": true,
    "PATHS": [{
      "PATH": "/vagrant/files_backup",
      "EXCLUDE_PATHS": [
        "cache"
      ],
      "EXCLUDE_PATTERNS" : [
        "*.git"
      ]
    },{
      "PATH": "/path/to/include",
      "EXCLUDE_PATHS": [],
      "EXCLUDE_PATTERNS" : [
        "*/.git",
        "*.sock",
        "*.lock",
        "*/*cache*",
        "*/node_modules"
      ]
    }]
  },
EOL
fi
if [[ "${BACKUP_DBS}" -ne 0 ]]; then
cat >>"${HERE}/client/backup/backup.json" <<EOL
  "DATABASE_BACKUP": {
    "ENABLED": false
  }
}
EOL
else
have_entry=0;
db_string=' "DATABASE_BACKUP": {
 "ENABLED": true,
 "DATABASES": ['
if [[ ${BACKUP_MONGO} -eq 0 ]]; then
  cd "${HERE}/client"
  vagrant ssh "${client_vm_name}" -c "sudo systemctl restart mongod"
  cd "${HERE}"
  if [[ ${BACKUP_ALL_DBS} -eq 0 ]]; then
    have_entry=1;
    db_string=${db_string}'
  {
   "DB_TYPE": "mongo",
   "DB_HOST":"127.0.0.1",
   "DB_PORT":27017,
   "DB_USER": "admin",
   "DB_PASSWORD": null,
   "DB_NAME": null,
   "AUTHENTICATION_DATABASE": ""
	}'
  elif [[ ${BACKUP_MONGO_AUTH} -eq 0 ]]; then
    add_mongo_user
    have_entry=1;
    db_string=${db_string}'
  {
   "DB_TYPE": "mongo",
   "DB_HOST":"127.0.0.1",
   "DB_PORT":27017,
   "DB_USER": "admin",
   "DB_PASSWORD": "'${DB_ADMIN_ROOT_PASSWD}'",
   "DB_NAME": "'${MONGO_DUMP_DB_NAME}'",
   "AUTHENTICATION_DATABASE": "admin"
	}'
	else
	  have_entry=1;
    db_string=${db_string}'
  {
   "DB_TYPE": "mongo",
   "DB_HOST":"127.0.0.1",
   "DB_PORT":27017,
   "DB_USER": "admin",
   "DB_PASSWORD": null,
   "DB_NAME": "'${MONGO_DUMP_DB_NAME}'",
   "AUTHENTICATION_DATABASE": ""
	}'
	fi
fi
if [[ ${BACKUP_POSTGRESQL} -eq 0 ]]; then
  cd "${HERE}/client"
  vagrant ssh "${client_vm_name}" -c "sudo systemctl restart postgresql"
  cd "${HERE}"
  if [[ ${have_entry} -gt 0 ]]; then
   db_string="${db_string},"
  fi
  have_entry=1;
  if [[ ${BACKUP_ALL_DBS} -eq 0 ]]; then
   db_string=${db_string}'
  {
   "DB_TYPE": "postgresql",
   "DB_HOST":"127.0.0.1",
   "DB_PORT":5432,
   "DB_USER": null,
   "DB_PASSWORD": null,
   "DB_NAME": null
	}'
	elif [[ ${BACKUP_POSTGRESQL_AUTH} -eq 0 ]]; then
	  add_postgres_user
    have_entry=1;
    db_string=${db_string}'
    {
      "DB_TYPE": "postgresql",
      "DB_HOST":"127.0.0.1",
      "DB_PORT":5432,
      "DB_USER": "'${DB_SERVICE_USER}'",
      "DB_PASSWORD": "'${DB_SERVICE_PASSWORD}'",
      "DB_NAME": "'${POSTGRESQL_DUMP_DB_NAME}'"
	  }'
  else
    have_entry=1;
    db_string=${db_string}'
    {
      "DB_TYPE": "postgresql",
      "DB_HOST":"127.0.0.1",
      "DB_PORT":5432,
      "DB_USER": null,
      "DB_PASSWORD": null,
      "DB_NAME": "'${POSTGRESQL_DUMP_DB_NAME}'"
	  }'
  fi
fi
if [[ ${BACKUP_MYSQL} -eq 0 || ${BACKUP_MARIADB} -eq 0 ]]; then
  my_maria="mysql"
  if [[ ${BACKUP_MYSQL} -eq 1 && ${BACKUP_MARIADB} -eq 0 ]];then
    my_maria="mariadb"
  fi
  systemd_name="${my_maria}"
  if [[ "${my_maria}" == "mysql" && "${CLIENT_OS}" == "centos" ]]; then
    systemd_name="mysqld"
  fi
  cd "${HERE}/client"
  vagrant ssh "${client_vm_name}" -c "sudo systemctl restart ${systemd_name}"
  cd "${HERE}"
  if [[ ${have_entry} -gt 0 ]]; then
   db_string="${db_string},"
  fi
  have_entry=1;
  if [[ ${BACKUP_ALL_DBS} -eq 0 ]]; then
   db_string=${db_string}'
  {
   "DB_TYPE": "'${my_maria}'",
   "DB_HOST":"localhost",
   "DB_PORT":3306,
   "DB_USER": "root",
   "DB_PASSWORD": "'${DB_ADMIN_ROOT_PASSWD}'",
   "DB_NAME": null
	}'
	elif [[ ${BACKUP_MYSQL_AUTH} -eq 0 ]]; then
	  add_mysql_mariadb_user
    have_entry=1;
    db_string=${db_string}'
    {
   "DB_TYPE": "'${my_maria}'",
   "DB_HOST":"localhost",
   "DB_PORT":3306,
   "DB_USER": "root",
   "DB_PASSWORD": "'${DB_ADMIN_ROOT_PASSWD}'",
   "DB_NAME": "'${MYSQL_DUMP_DB_NAME}'"
	}'
	else
	  have_entry=1;
    db_string=${db_string}'
    {
   "DB_TYPE": "'${my_maria}'",
   "DB_HOST":"localhost",
   "DB_PORT":3306,
   "DB_USER": "root",
   "DB_PASSWORD": "'${DB_ADMIN_ROOT_PASSWD}'",
   "DB_NAME": "'${MYSQL_DUMP_DB_NAME}'"
	}'
  fi
fi
db_string=${db_string}'
  ]
 }
}
'
echo "${db_string}" >>"${HERE}/client/backup/backup.json"
fi
}


function client_backup() {
backup_type="${1}"
if [[ ! -z "${backup_type}" ]]; then
    if [[ "${backup_type}" == "files" ]]; then
      sed  -i 's|BACKUP_FILES=\(.*\)|BACKUP_FILES=0|' "${HERE}/.env"
      sed  -i 's|BACKUP_DBS=\(.*\)|BACKUP_DBS=1|' "${HERE}/.env"
    elif [[ "${backup_type}" == "databases" ]]; then
      sed  -i 's|BACKUP_FILES=\(.*\)|BACKUP_FILES=1|' "${HERE}/.env"
      sed  -i 's|BACKUP_DBS=\(.*\)|BACKUP_DBS=0|' "${HERE}/.env"
    elif [[ "${backup_type}" == "all" ]]; then
      sed  -i 's|BACKUP_FILES=\(.*\)|BACKUP_FILES=0|' "${HERE}/.env"
      sed  -i 's|BACKUP_DBS=\(.*\)|BACKUP_DBS=0|' "${HERE}/.env"
    fi
fi
set_files
make_backup_json
_client_run_script "${HERE}/client/backup/backup.sh"
}

function client_restore() {
. "${HERE}/.env"
if [[ -d "${HERE}/client/backups" ]]; then
  if ls ${HERE}/client/backups/* 1>/dev/null 2>&1; then
    restore_folder="$(cd "$(\ls -1dt ${HERE}/client/backups/* | head -n 1)" && pwd -P)"
    restore_folder="$( echo ${restore_folder} | sed "s|${HERE}/client|/vagrant|")"
    restore_type="${1}"
    if [[ ! -z "${restore_type}" ]]; then
        if [[ "${restore_type}" == "files" ]]; then
          sed  -i 's|BACKUP_FILES=\(.*\)|BACKUP_FILES=0|' "${HERE}/.env"
          sed  -i 's|BACKUP_DBS=\(.*\)|BACKUP_DBS=1|' "${HERE}/.env"
        elif [[ "${restore_type}" == "databases" ]]; then
          sed  -i 's|BACKUP_FILES=\(.*\)|BACKUP_FILES=1|' "${HERE}/.env"
          sed  -i 's|BACKUP_DBS=\(.*\)|BACKUP_DBS=0|' "${HERE}/.env"
        elif [[ "${restore_type}" == "all" ]]; then
          sed  -i 's|BACKUP_FILES=\(.*\)|BACKUP_FILES=0|' "${HERE}/.env"
          sed  -i 's|BACKUP_DBS=\(.*\)|BACKUP_DBS=0|' "${HERE}/.env"
        fi
    fi
    set_files
    if [[ "${BACKUP_FILES}" -eq 0 ]]; then
      mkdir -p ${HERE}/client/files_backup
      if ls ${HERE}/client/files_backup/* 1>/dev/null 2>&1; then
          rm -rf ${HERE}/client/files_backup/*
      fi
      if [[ -d "${HERE}/client/files_backup/.git" ]]; then
        rm -rf "${HERE}/client/files_backup/.git"
      fi
      ls -al "${HERE}/client/files_backup/"
    fi
    make_backup_json
    _client_run_script "${HERE}/client/backup/restore.sh" "${restore_folder}"
    ls -al "${HERE}/client/files_backup/"
  fi
else
  echo "No path to send for restore"
fi

}

case "$1" in
  install)
    if [[ ! "$#" -eq 2 ]]; then
      echo_usage
      exit 1;
    fi
    client_install "$2"
    ;;
  import)
    if [[ ! "$#" -eq 2 ]]; then
      echo_usage
      exit 1;
    fi
    client_import "$2"
    ;;
  make)
  destroy_client_vm
  sleep 2
  if [[ "$#" -gt 1 ]]; then
    if [[ "${2}" == "ubuntu" || "${2}" == "centos" ]]; then
      CLIENT_OS="${2}"
      sed  -i 's|CLIENT_OS=\(.*\)|CLIENT_OS=\"'${CLIENT_OS}'\"|' "${HERE}/.env"
      if [[ "$#" -eq 3 ]]; then
        if [[ "${3}" == "xenial" || "${3}" == "bionic" ]]; then
          CLIENT_UBUNTU_FLAVOR="${3}64"
          sed  -i 's|CLIENT_UBUNTU_FLAVOR=\(.*\)|CLIENT_UBUNTU_FLAVOR=\"'${CLIENT_UBUNTU_FLAVOR}'\"|' "${HERE}/.env"
        fi
      fi
   fi
  fi
  make_client_vm
  ;;
  backup)
    client_backup "${2}"
  ;;
  restore)
    client_restore "${2}"
  ;;
  stop)
    stop_client_vm
    ;;
  restart)
    stop_client_vm
    start_client_vm
    ;;
  start)
    start_client_vm
    ;;
  status)
    client_vm_status
    ;;
  destroy)
    destroy_client_vm
    ;;
  ssh)
    ssh_client_vm
    ;;
  *)
    echo_usage;;
esac
