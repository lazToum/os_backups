#!/bin/bash
if [[ "$(whoami)" != "root" ]]
  then echo "Run as root or with sudo -u"
  exit
fi
HERE=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
DB_PATH=/data
ENV_EXISTS=0
DB_SERVICE_USER=${DB_SERVICE_USER:-app_db}
DB_PASSWORD=${DB_PASSWORD:-app_db}
MONGO_DUMP_DB_NAME=${MONGO_DUMP_DB_NAME:-test}
MONGO_GROUP=${MONGO_GROUP:-mongodb}
MONGO_USER=${MONGO_USER:-mongodb}

if [[ -f "${HERE}/.mongo.env" ]]; then
    . "${HERE}/.mongo.env"
    DB_PORT=${DB_PORT:-27017}
    mkdir -p ${DB_PATH}/db
    cmd_args=(--dbpath $DB_PATH/db --port $DB_PORT --logpath $DB_PATH/logs/mongodb.log --logRotate reopen --verbose vvvvv --bind_ip_all --logappend)
    cmd_to_run="mongod \
    --dbpath $DB_PATH/db \
    --port $DB_PORT \
    --logpath $DB_PATH/logs/mongodb.log \
    --logRotate reopen \
    --verbose vvvvv \
    --bind_ip_all \
    --logappend"
    create_admin_user=0
    create_app_user=0


    user_and_group() {
        if ! grep -q "$MONGO_GROUP" /etc/group; then
            groupadd -r "$MONGO_GROUP"
        fi
        if ! id -u "$MONGO_USER" >/dev/null 2>&1; then
            useradd -r -g "$MONGO_GROUP" "$MONGO_USER"
        fi
        chown -R "$MONGO_USER" "$DB_PATH"
        chgrp -R "$MONGO_GROUP" "$DB_PATH"
        chmod -R 0755 "$DB_PATH"
    }

    user_and_group
        mkdir -p "$DB_PATH"/logs
#    if [ ! -e $DB_PATH/logs/mongodb.log ]; then # no not then, clear the logs either way
        echo "" > "$DB_PATH"/logs/mongodb.log
        chown "$MONGO_USER" "$DB_PATH"/logs/mongodb.log
        chgrp "$MONGO_GROUP" "$DB_PATH"/logs/mongodb.log
#    fi

    _create_admin_user() {
       DB_ADMIN_ROOT_PASSWD=${DB_ADMIN_ROOT_PASSWD:-admin}
       MONGO_ADMIN_USER_PASSWORD=${DB_ADMIN_ROOT_PASSWD:-admin}
       # safe to drop (to avoid error on creating)
       mongo admin --quiet --eval "db.dropUser('admin')" > /dev/null
       mongo admin --quiet --eval "db.createUser({user: 'admin', pwd: '$DB_ADMIN_ROOT_PASSWD', roles:['root']});"
    }

    _create_app_user() {
       # safe to drop (to avoid error on creating)
       mongo "$MONGO_DUMP_DB_NAME" --quiet --eval "db.dropUser('$DB_SERVICE_USER')" > /dev/null
       mongo "$MONGO_DUMP_DB_NAME" --quiet --eval "db.createUser({user: '$DB_SERVICE_USER', pwd: '$DB_PASSWORD', roles:[{role:'dbAdmin',db:'"$MONGO_DUMP_DB_NAME"'}, {role:'readWrite',db:'"$MONGO_DUMP_DB_NAME"'}, {role:'backup',db:'admin'}]});"
    }
    create_user_s_and_shutdown() {
        if [[ ${create_admin_user} -eq 1 ]]; then
            echo "=> Creating admin user"
            _create_admin_user
        fi
        if [[ ${create_app_user} -eq 1 ]]; then
            echo "=> Creating application user"
            _create_app_user
        fi
        # shutdown
        if [[ "$(sudo mongod --shutdown)" ]]; then
          echo "restarting..."
        fi
    }

    check_auth() {
        create_users=0
        echo "=> Temporarily starting to check auth"
        # temp cmd with --noauth
        setup_cmd="$cmd_to_run \
         --quiet --noauth"

        # the default cmd to run on restart
        cmd_to_run="$cmd_to_run --quiet --auth"
        # start mongod without auth
        sudo -u ${MONGO_USER} mongod ${cmd_args[@]} --noauth --fork
#        echo $MONGO_USER
        # Wait to boot
        RET=1
        RETRIES=10
        while [[ ${RET} -ne 0 && ${RETRIES} -gt 0 ]]; do
            echo "=> Waiting for confirmation of MongoDB service startup..."
            sleep 5
            mongo admin --eval "help" > /dev/null 2>&1
            RET=$?
            RETRIES=$((RETRIES - 1))
        done
        if [[ ${RETRIES} -eq 0 && ${RET} -ne 0 ]]; then
          echo "Service did not start"
          exit 1
        fi
        echo "=> Ensuring env users have the correct credentials"
        x=`mongo admin --quiet --eval "db.auth({user: 'admin', pwd: '$DB_ADMIN_ROOT_PASSWD'});" | grep -E 0\|1`
        if [[ "$x" == "0" ]]; then
            create_users=1
            create_admin_user=1
        fi
        x=`mongo "$MONGO_DUMP_DB_NAME" --quiet --eval "db.auth({user: '$DB_SERVICE_USER', pwd: '$DB_PASSWORD'});" | grep -E 0\|1`
        if [[ "$x" == "0" ]]; then
            create_users=1
            create_app_user=1
        fi
        if [[ ${create_users} -eq 1 ]]; then
            create_user_s_and_shutdown
            sleep 5;
            cmd_to_run=$(echo "$cmd_to_run" | sed 's|noauth|auth|')
            start
        else
            echo "=> No need to create any user, env users already created with correct credentials"
            echo "=> Restarting in --auth mode ..."
            if [[ "`uname`" == 'Darwin' ]]; then
              pkill mongd
            else
             sudo mongod --shutdown
            fi
            sleep 5;
              cmd_to_run=$(echo "$cmd_to_run" | sed 's|noauth|auth|')
            start
        fi
    }

    start() {
        echo "=> Starting mongod as user $MONGO_USER ..."
        sudo -u "$MONGO_USER" ${cmd_to_run} --fork
    }
    if pgrep mongod > /dev/null 2>&1 ; then
      echo "stopping mongod ..."
      pkill mongod && sleep 2
   fi
    BACKUP_MONGO_AUTH=${BACKUP_MONGO_AUTH:-1}
    if [[ ${BACKUP_MONGO_AUTH} -eq 0 ]]; then
        check_auth
    else
        cmd_to_run= "$cmd_to_run" --quiet --noauth
        start
    fi
fi
