#!/bin/bash

set -e

if [[ ! -x "$(command -v vagrant)" ]];then
  echo "Command vagrant not found. Is it installed?"
  exit 1
fi
if [[ ! -x "$(command -v virtualbox)" ]];then
  echo "Virtualbox not found. Is it installed?"
  exit 1
fi
HERE=$(cd -P -- "$(dirname -- "$0")" && pwd -P)

cd "${HERE}"
if [[ ! -f "${HERE}/.env" ]];then
  echo "Please include a .env file containing SHARED_SSH_USER, SHARED_SECRET, OAUTH_VALIDATION_URL ... variables"
  exit 1
fi
. "${HERE}/.env"

SHARED_SECRET=${SHARED_SECRET:-}
SHARED_SSH_USER="keyman"
OAUTH_VALIDATION_URL=${OAUTH_VALIDATION_URL:-}
BACKUP_SERVER_OS="${BACKUP_SERVER_OS:-centos}"
KEYS_PROXY_SERVER_OS="${KEYS_PROXY_SERVER_OS:-ubuntu}"
BACKUP_SERVER_IP="${BACKUP_SERVER_IP:-192.168.56.101}"
KEYS_PROXY_SERVER_IP="${KEYS_PROXY_SERVER_IP:-192.168.56.102}"
UBUNTU_SERVER_FLAVOR="${UBUNTU_SERVER_FLAVOR:-xenial64}"
SSH_PORT="${SSH_PORT:-2244}"
NGINX_PORT="${NGINX_PORT:-7678}"

backup_server_name="backup_server"
keys_proxy_server_name="keys_proxy"
backup_and_proxy_server="backup_and_proxy_server"



if [[ -d "${HERE}/servers" ]]; then
  mkdir -p "${HERE}/servers"
fi

mkdir -p "${HERE}/servers/proxy"
mkdir -p "${HERE}/servers/backup"

function _clear() {
if [[ -f "${HERE}/servers/backup/.env" ]]; then
  rm "${HERE}/servers/backup/.env"
fi
if [[ -f "${HERE}/servers/proxy/.env" ]]; then
  rm "${HERE}/servers/proxy/.env"
fi
if [[ -f "${HERE}/servers/Vagrantfile" ]]; then
  rm "${HERE}/servers/Vagrantfile"
fi
if [[ -f "${HERE}/servers/backup/install.sh" ]]; then
  rm "${HERE}/servers/backup/install.sh"
fi
if [[ -f "${HERE}/servers/proxy/install.sh" ]]; then
  rm "${HERE}/servers/proxy/install.sh"
fi
if ls ${HERE}/servers/.vagrant/machines/* 1>/dev/null 2>&1; then
  rm -rf ${HERE}/servers/.vagrant/machines/*
fi
}

function set_files() {
cp ${HERE}/../backup_server/install.sh "${HERE}/servers/backup/install.sh"
chmod +x "${HERE}/servers/backup/install.sh"
cp ${HERE}/../keys_proxy/install.sh "${HERE}/servers/proxy/install.sh"
chmod +x "${HERE}/servers/proxy/install.sh"
  cat >"${HERE}/servers/backup/.env" <<EOL
SSH_PORT=${SSH_PORT}
SSH_KEYS_USER=${SHARED_SSH_USER}
QUOTA_GB_SIZE=2
EOL

  cat >"${HERE}/servers/proxy/.env" <<EOL
SHARED_SECRET=${SHARED_SECRET}
OAUTH_VALIDATION_URL=${OAUTH_VALIDATION_URL}
NGINX_PORT=${NGINX_PORT}
SERVICE_USER=${SHARED_SSH_USER}
INSTALL_CERTBOT=OFF
SERVER_NAME=""
EOL
}

function check_guest_port() {
if [[ -f "${HERE}/servers/Vagrantfile" ]]; then
  if [[ -z "$(cat ${HERE}/servers/Vagrantfile | grep ssh.guest_port || echo )" ]]; then
  vm_name="$(cat ${HERE}/servers/Vagrantfile| grep 'private_network, ip: "'${BACKUP_SERVER_IP}'"' | cut -d . -f1)"
  sed -i "/vm.network :private_network, ip: \"${BACKUP_SERVER_IP}\"/a${vm_name}.ssh.guest_port = ${SSH_PORT}" "${HERE}/servers/Vagrantfile"
  fi
fi
}


function transfer_proxy_key_to_backup_server() {
ssh vagrant@${KEYS_PROXY_SERVER_IP} -p22 -i "${HERE}/servers/.vagrant/machines/${keys_proxy_server_name}/virtualbox/private_key" "sudo bash -c 'cat /home/${SHARED_SSH_USER}/.ssh/id_rsa.pub > /vagrant/${SHARED_SSH_USER}.pub'"
echo "Checking if file /vagrant/${SHARED_SSH_USER}.pub exists in proxy_keys server...."
COUNTER=0
# at most ten retries to wait for ${SHARED_SSH_USER}.pub
while [[ ${COUNTER} -lt 10 ]]; do
  sleep 2
  echo "Retrying..."
  if [[ -f "${HERE}/servers/${SHARED_SSH_USER}.pub" ]]; then
    COUNTER=$((COUNTER+10))
  else
    COUNTER=$((COUNTER+1))
  fi
done
if [[ -f "${HERE}/servers/${SHARED_SSH_USER}.pub" ]]; then
echo "file /vagrant/${SHARED_SSH_USER}.pub exists... adding it to backup_server's /home/${SHARED_SSH_USER}/.ssh/authorized_keys (the proxy server now has access to backup server)"
ssh vagrant@${BACKUP_SERVER_IP} -p ${SSH_PORT} -i "${HERE}/servers/.vagrant/machines/${backup_server_name}/virtualbox/private_key" "sudo mkdir -p /home/${SHARED_SSH_USER}/.ssh/ && sudo bash -c 'cat /vagrant/${SHARED_SSH_USER}.pub >> /home/${SHARED_SSH_USER}/.ssh/authorized_keys'"
fi
check_guest_port
}


function make_different_servers() {
backup_box="centos/7"
if [[ "${BACKUP_SERVER_OS}" == "ubuntu" ]]; then
  backup_box="ubuntu/${UBUNTU_SERVER_FLAVOR}"
fi
proxy_box="ubuntu/${UBUNTU_SERVER_FLAVOR}"
if [[ "${KEYS_PROXY_SERVER_OS}" == "centos" ]]; then
  proxy_box="centos/7"
fi
  cat >"${HERE}/servers/Vagrantfile" <<EOL
Vagrant.configure("2") do |config|
  config.vm.define "backup_server" do |backup_server|
    backup_server.vm.box = "${backup_box}"
    backup_server.vm.network :private_network, ip: "${BACKUP_SERVER_IP}"
    backup_server.vm.network :forwarded_port, guest: ${SSH_PORT}, host: ${SSH_PORT}
    backup_server.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--memory", 512]
      v.customize ["modifyvm", :id, "--name", "${backup_server_name}"]
    end
    backup_server.vm.provision "shell" do |s|
      s.inline ="sudo /vagrant/backup/install.sh"
    end
  end
  config.vm.define "keys_proxy" do |keys_proxy|
    keys_proxy.vm.box = "${proxy_box}"
    keys_proxy.vm.network :private_network, ip: "${KEYS_PROXY_SERVER_IP}"
    keys_proxy.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--memory", 512]
      v.customize ["modifyvm", :id, "--name", "${keys_proxy_server_name}"]
    end
    keys_proxy.vm.provision "shell" do |s|
      s.inline ="sudo /vagrant/proxy/install.sh"
    end
  end
end
EOL
cd "${HERE}/servers"
vagrant up
cd "${HERE}"
transfer_proxy_key_to_backup_server
}

function make_one_server() {
box_os="ubuntu/${UBUNTU_SERVER_FLAVOR}"
if [[ "${KEYS_PROXY_SERVER_OS}" == "centos" ]]; then
  box_os="centos/7"
fi
  cat >"${HERE}/servers/Vagrantfile" <<EOL
Vagrant.configure("2") do |config|
  config.vm.define "backup_and_proxy_server" do |backup_and_proxy_server|
    backup_and_proxy_server.vm.box = "${box_os}"
    backup_and_proxy_server.vm.network :private_network, ip: "${BACKUP_SERVER_IP}"
    backup_and_proxy_server.vm.network :forwarded_port, guest: ${SSH_PORT}, host: ${SSH_PORT}
    backup_and_proxy_server.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--memory", 512]
      v.customize ["modifyvm", :id, "--name", "${backup_and_proxy_server}"]
    end
    backup_and_proxy_server.vm.provision "shell" do |s|
      s.inline ="sudo /vagrant/backup/install.sh && sudo /vagrant/proxy/install.sh"
    end
  end
end
EOL
cd "${HERE}/servers"
vagrant up
cd "${HERE}"
sleep 5
ssh vagrant@${BACKUP_SERVER_IP} -p ${SSH_PORT} -i "${HERE}/servers/.vagrant/machines/${backup_and_proxy_server}/virtualbox/private_key" "sudo mkdir -p /home/${SHARED_SSH_USER}/.ssh/ && sudo bash -c 'cat /home/${SHARED_SSH_USER}/.ssh/id_rsa.pub > /home/${SHARED_SSH_USER}/.ssh/authorized_keys'"
check_guest_port
}

function make_servers() {
  set_files
  if [[ "${BACKUP_SERVER_IP}" == "${KEYS_PROXY_SERVER_IP}" ]];then
    make_one_server
  else
    make_different_servers
  fi
}

function destroy_servers() {
  if [[ -f "${HERE}/servers/Vagrantfile" ]]; then
    cd "${HERE}/servers"
    vagrant destroy -f
    cd "${HERE}"
  fi
  _clear
}

function echo_usage() {
  echo  -n "Usage: ${0} make|status|start|stop|restart|destroy
      Creates, starts, stops or destroys server (backup|proxy) vms
"
}

function stop() {
if [[ -f "${HERE}/servers/Vagrantfile" ]]; then
    cd "${HERE}/servers"
    vm_names=( $(vagrant status | awk '{print $1}' | grep "${backup_and_proxy_server}\|${backup_server_name}\|${keys_proxy_server_name}" || echo '' ) )
    vm_names_len=${#vm_names[@]}
    if [[ ${vm_names_len} -gt 0 ]]; then
      for (( i=0; i<${vm_names_len}; i++ ));
      do
        vm_name=${vm_names[$i]}
        if [[ ! -z "$(vagrant status "${vm_name}" | awk '{print $2}' | grep 'running')" ]]; then
          if [[ "${vm_name}" == "${keys_proxy_server_name}" ]];then
            ssh vagrant@${KEYS_PROXY_SERVER_IP} -p 22 -i "${HERE}/servers/.vagrant/machines/${vm_name}/virtualbox/private_key" "sudo poweroff" || echo
          elif [[ "${vm_name}" == "${backup_server_name}" ]];then
            ssh vagrant@${BACKUP_SERVER_IP} -p "${SSH_PORT}" -i "${HERE}/servers/.vagrant/machines/${vm_name}/virtualbox/private_key" "sudo poweroff" || echo
          else
            ssh vagrant@${BACKUP_SERVER_IP} -p "${SSH_PORT}" -i "${HERE}/servers/.vagrant/machines/${vm_name}/virtualbox/private_key" "sudo poweroff" || echo
          fi
        fi
      done
    fi
fi
cd "${HERE}"
}

function status() {
  if [[ -f "${HERE}/servers/Vagrantfile" ]]; then
    cd "${HERE}/servers"
    vagrant status | grep "${backup_and_proxy_server}\|${backup_server_name}\|${keys_proxy_server_name}"
  else
    echo "No Vagrantfile in folder ${HERE}/servers... No vms created or they have been destroyed"
  fi;
  cd "${HERE}"
}

function start() {
if [[ -f "${HERE}/servers/Vagrantfile" ]]; then
    cd "${HERE}/servers"
    vm_names=( $(vagrant status | awk '{print $1}' | grep "${backup_and_proxy_server}\|${backup_server_name}\|${keys_proxy_server_name}" || echo '' ) )
    vm_names_len=${#vm_names[@]}
    if [[ ${vm_names_len} -gt 0 ]]; then
      for (( i=0; i<${vm_names_len}; i++ ));
      do
        vm_name=${vm_names[$i]}
        if [[ ! -z "$(vagrant status "${vm_name}" | awk '{print $2}' | grep 'aborted')" || "$(vagrant status "${vm_name}" | awk '{print $2}' | grep 'poweroff')" ]]; then
          vagrant up --no-provision "${vm_name}"
        fi
      done
    fi
fi
cd "${HERE}"
}

function server_ssh() {
  if [[ -f "${HERE}/servers/Vagrantfile" ]]; then
      cd "${HERE}/servers"
    if [[ "$#" -eq 0 ]]; then
      if [[ "${BACKUP_SERVER_IP}" == "${KEYS_PROXY_SERVER_IP}" ]];then
        vagrant ssh ${backup_and_proxy_server}
      fi
    fi
    else
      echo "No Vagrantfile in folder ${HERE}/servers... No vms created or they have been destroyed"
  fi
  cd "${HERE}"
}

check_guest_port

case "$1" in
  make)
  destroy_servers
  sleep 2
  make_servers
  ;;
  stop)
    stop;;
  start)
    start;;
  restart)
    stop
    start;;
  status)
    status;;
  destroy)
    destroy_servers;;
  ssh)
    server_ssh;;
  *)
    echo_usage;;
esac

exit 0
