# Flow for rsync over ssh using public key authentication

![Backup Flow](./backup_flow.jpg "Backup Flow")

On the first run, if the backup user's public key is not included in the backup server's authorized_keys, the script tries to send this key to the keys proxy.\
A shared secret and / or an OAuth server can be used for secure communication between the backup client and the keys proxy.\
If the latter has access to the backup server as a specific user, it adds this key to this user's authorized keys.\
The backup server watches this file (/home/proxy_user/.ssh/authorized_keys) and on change it creates (if necessary) the vm's user and directory for backups.

## Status

- [x] Proxy server
  - [x] Install on Centos 7
  - [x] Install on Ubuntu 18.04
  - [x] Install on Ubuntu 16.04
  - [x] Communication (after token verification) with backup server
- [ ] Backup server
  - [x] Install on Centos 7, Ubuntu 18.04, Ubuntu 16.04 
  - [x] ssh configuration (only allow rsync with public-key ssh connection)
  - [x] systemd.path and systemd.service to listen for changes in `/home/proxy_user/.ssh/authorized_keys`
  - [x] Users creation on above changes
  - [ ] Quota (limit folder size for each user)
- [ ] OAuth server (with 'grant_type': 'client_credentials')
- [ ] Backup client
  - [x] systemd.timer and systemd.service for backup scheduling
  - [x] Backup user (+ssh keypair) creation
  - [x] Communication with keys proxy (if cannot rsync to backup server)
  - [x] Directory backups
  - [x] Database backups
    - [x] mongodb
    - [x] postgresql
    - [x] mysql | mariadb  
  - [x] Directory restores
  - [x] Database restores
    - [x] mongodb
    - [x] postgresql
    - [x] mysql | mariadb
  - [ ] option to encrypt backups using pgp (if available on client)
- [ ] Automated tests
  - [x] Backup servers installation (Ubuntu 16/18.04, CentOS 7)
  - [x] Client installation (Ubuntu 16/18.04, CentOS 7)
    - [x] Basic Installation (Ubuntu 16/18.04, CentOS 7)
    - [x] Database Setup
      - [x] mongodb (Ubuntu 16/18.04, CentOS 7)
        - [x] Installation
        - [x] Import sample data
      - [x] postgresql (Ubuntu 16/18.04, CentOS 7)
        - [x] Installation
        - [x] Import sample data
      - [x] mysql (Ubuntu 16/18.04, CentOS 7)
        - [x] Installation
        - [x] Import sample data
      - [x] mariadb (Ubuntu 16/18.04, CentOS 7)
        - [x] Installation
        - [x] Import sample data
  - [x] Directory backup (include/exclude/patterns)
  - [x] Directory restore
  - [ ] Database backup
    - [x] mongodb
      - [x] all databases
      - [x] single db, no authentication
      - [x] single db, with authentication
    - [ ] postgresql
      - [x] all databases
      - [x] single db, no authentication
      - [ ] single db, with authentication
    - [x] mysql
      - [x] all databases
      - [x] single db, no authentication
      - [x] single db, with authentication
    - [x] mariadb
      - [x] all databases
      - [x] single db, no authentication
      - [x] single db, with authentication
  - [ ] Database restore
    - [x] mongodb
      - [x] all databases
      - [x] single db, no authentication
      - [x] single db, with authentication
    - [ ] postgresql
      - [x] all databases
      - [x] single db, no authentication
      - [ ] single db, with authentication
    - [x] mysql
      - [x] all databases
      - [x] single db, no authentication (root authentication)
      - [x] single db, with authentication
    - [x] mariadb
      - [x] all databases
      - [x] single db, no authentication (root authentication)
      - [x] single db, with authentication

## Backup client

Required commands: [rsync](https://rsync.samba.org/) for communication with the backup server, [mongodump](https://docs.mongodb.com/manual/reference/program/mongodump/) and [mongorestore](https://docs.mongodb.com/manual/reference/program/mongorestore/) (version >= 3.4), for mongodb databases backup and restore, [mysqldump](https://dev.mysql.com/doc/en/mysqldump.html) and [mysql](https://dev.mysql.com/doc/en/mysql.html), for mysql | mariadb databases backup and restore, [pg_dump](https://www.postgresql.org/docs/current/app-pgdump.html) and [pg_restore](https://www.postgresql.org/docs/current/app-pgrestore.html) for postgresql databases backup and restore. \
Usage:
Create a [backup.json](client/backup.json.example)
(```cp client/backup.json.example client/backup.json```)
with the proxy, backup server settings and the backup files and database options.\
Note that for database dumps, the arguments that are used are:

- ```mysqldump --hex-blob --routines --triggers --user=the_user --password=the_password --databases the_db (or --all-databases if no DB_NAME is supplied) | gzip > /path/to/the_dump.gz```

- ```PGPASSWORD=passwdifany pg_dump(or pg_dumpall if no DB_NAME) --clean --if-exists --username=the_user --dbname=the_db(if DB_NAME) | gzip > /path/to/the_dump.gz```

- ```mongodump --gzip --quiet --username=the_user --password=the_password --db=the_db(if any) --authenticationDatabase=the_auth_db(if any) --archive=/path/to/the_dump.gz```

Feel free to either modify them in [backup.sh#L607](client/backup.sh#L607) or use your own database backup/restore mechanism and just include the (folders containing the) dumps in the FILES_BACKUP section.

Mandatory entries:

- BACKUP_REMOTE_SSH_HOST : the actual backup destination

- BACKUP_REMOTE_SSH_PORT : the port to use for ssh connection

- KEYS_SERVER_HTTP_HOST : the (keys proxy server) host to send the ssh public key (if not already)

- KEYS_SERVER_HTTP_PORT : the HTTP port of the above server

- OAUTH_CLIENT_ID : 0Auth client_id for communication between the client and the keys proxy (bearer access_token)

- OAUTH_CLIENT_SECRET : 0Auth client_secret for communication between the client and the keys proxy  (bearer access_token)

- SHARED_SECRET : a shared token between the client and the keys proxy

- FILES_BACKUP : (if enabled) specify the folders you want to include and the folders and patterns to exclude

- DATABASE_BACKUP : (if enabled) specify the connection settings (host, port, authentication) and the databases you want to backup.

For the entry: ```"TIMER_CALENDAR": "Sun *-*-* 00:00:00"```, (Every Sunday on 00:00:00, format: ```DayOfWeek Year-Month-Day Hour:Minute:Second```), you can use ```man systemd.timer``` and ```man systemd.time``` for available options and examples.

```[json]

{
  "TIMER_CALENDAR": "Sun *-*-* 00:00:00",
  "BACKUPS_FOLDER": "/backups",
  "BACKUPS_TO_KEEP": 3,
  "PARTNER": "partner_name",
  "BACKUP_LOCAL_USER": "backer",
  "KEYS_SERVER_HTTP_HOST":"http://XXX.XXX.XXX.XXX",
  "KEYS_SERVER_HTTP_PORT":8089,
  "BACKUP_REMOTE_SSH_HOST":"YYY.YYY.YYY.YYY",
  "BACKUP_REMOTE_SSH_PORT":2244,
  "OAUTH_TOKEN_HOST":"https://example.com/oauth/token",
  "OAUTH_CLIENT_ID" : "replacewithoauthclientid",
  "OAUTH_CLIENT_SECRET" : "replacewithoauthclientsecret",
  "SHARED_SECRET": "replacewithsharedservicesecret",
  "FILES_BACKUP": {
    "ENABLED": true,
    "PATHS": [{
      "PATH": "/var/something",
      "EXCLUDE_PATHS": [
        "/var/something/to/exclude1",
        "/or/relative/from/var/something/to/exclude"
      ],
      "EXCLUDE_PATTERNS" : [
        "*/.idea",
        "*/.vscode"
      ]
    },{
      "PATH": "/path/to/include",
      "EXCLUDE_PATHS": [],
      "EXCLUDE_PATTERNS" : [
        "*/.git",
        "*.sock",
        "*.lock",
        "*/*cache*",
        "*/node_modules"
      ]
    }]
  },
  "DATABASE_BACKUP": {
    "ENABLED": true,
    "DATABASES": [{
      "DB_TYPE": "mongo|postgresql|mysql|mariadb",
      "DB_HOST":"127.0.0.1",
      "DB_PORT":27017|5432|3306|...,
      "DB_USER": "admin",
      "DB_PASSWORD": null,
      "DB_NAME": "leave_empty_or_set_null_for_all",
      "AUTHENTICATION_DATABASE": "if_mongo_and_auth_else_remove_it_leave_it_empty_or_set_null"
    }]
  }
}

```

## Keys Proxy

Variables (to be inserted in [.env](keys_proxy/.env.example) file): ```cp keys_proxy/.env.example keys_proxy/.env```

```[bash]

SHARED_SECRET="replacewithsharedservicesecret"
OAUTH_VALIDATION_URL="https://example.com/oauth/validate"
NGINX_PORT=8089

```

## Backup server

Variables (to be inserted in [.env](backup_server/.env.example) file):```cp backup_server/.env.example backup_server/.env```

```[bash]

SSH_PORT=2244

```

## Tests

Variables (to be inserted in [.env](tests/.env.example) file):```cp tests/.env.example tests/.env```

```[bash]

SHARED_SECRET="replacewithsharedservicesecret"
OAUTH_VALIDATION_URL="https://example.com/oauth/validate"
BACKUP_SERVER_OS="ubuntu"
KEYS_PROXY_SERVER_OS="centos"
UBUNTU_SERVER_FLAVOR="bionic64"
BACKUP_SERVER_IP="192.168.56.101"
KEYS_PROXY_SERVER_IP="192.168.56.102"
SSH_PORT=2244
NGINX_PORT=8089
CLIENT_IP="192.168.56.105"
CLIENT_OS="centos"
CLIENT_UBUNTU_FLAVOR="bionic64"
DB_ADMIN_ROOT_PASSWD="admin_root_password"
DB_SERVICE_USER="service_user"
DB_SERVICE_PASSWORD="service_password"

```

No OAuth server implementation is included (yet?).\
If [vagrant](https://www.vagrantup.com/) and [virtualbox](https://www.virtualbox.org/) are installed, you can try the [servers.sh](tests/servers.sh), [client.sh](tests/client.sh) or [run.sh](tests/run.sh) scripts. They create (client and server) Vagrantfiles and test the connection, backup and restore process.\
For the database tests, the sample databases (not included here) [dbkoda-data](https://github.com/SouthbankSoftware/dbkoda-data) for mongodb, [sakila](https://dev.mysql.com/doc/sakila/en/) for mysql/mariadb, and [dvdrental](http://www.postgresqltutorial.com/postgresql-sample-database/) for postgresql, were used.\
Note that database installations in tests are not meant to be used in production. They are just installed for testing (for example: mysql_secure_installation is not executed).


##

Although not recommended, the OAuth server, keys proxy and backup server can all be on the same machine.\
Client, backup and proxy servers are tested on Ubuntu 16.04, Ubuntu 18.04 and CentOS 7\
Comments, issues, recommendations are more than welcome.
