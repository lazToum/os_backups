#!/bin/bash
set -e
HERE=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
cd ${HERE}
if [[ ! -f ${HERE}/.env ]];then
  echo "Please include a .env file containing the SHARED_SECRET, NGINX_PORT and OAUTH_VALIDATION_URL variables : SHARED_SECRET=......"
  exit 1
fi
. ${HERE}/.env
SERVICE_USER=${SERVICE_USER:-keyman}
FLASK_APP_FOLDER=${FLASK_APP_FOLDER:-/home/${SERVICE_USER}/keys_server}
NGINX_PORT=${NGINX_PORT:-7868}
DISTRO=""

function check_root() {
    if [[ "$(whoami)" != "root" ]]
      then echo "Run as root or with sudo"
      exit 1
    fi
}

function setup_centos() {
  if [[ ! $(rpm -qa | grep "epel-release") ]]; then
    yum install epel-release -y
  fi
  if [[ ! -x "$(command -v curl)" ]]; then
    yum install curl -y
  fi
  if [[ ! -x "$(command -v wget)" ]]; then
    yum install wget -y
  fi
 if [[ ! -x "$(command -v python3)" ]]; then
    yum install python36 -y
    ln -s /usr/bin/python36 /usr/bin/python3
  fi
  if [[ ! -x "$(command -v nginx)" ]]; then
    yum install nginx -y
  fi
  if [[ ! -x "$(command -v semanage)" ]]; then
    yum install policycoreutils-python -y
  fi
  if [[ ! -x "$(command -v gcc)" ]]; then
    yum install -y gcc
  fi
  if [[  ! -x "$(command -v virtualenv)" ]]; then
    yum install -y python36-devel
  fi
  if [[ ! "$(systemctl is-active firewalld)" == "active" ]]; then
    systemctl start firewalld
  fi
  if [[ ! "$(systemctl is-enabled firewalld)" == "enabled" ]]; then
    systemctl enable firewalld
  fi
  if [[ ! $(semanage port -l | grep ${NGINX_PORT}) ]]; then
    semanage port -a -t http_port_t -p tcp ${NGINX_PORT}
    setsebool -P httpd_can_network_connect 1
    semanage permissive -a httpd_t
  fi
  firewall-cmd --permanent --zone=public --add-port=${NGINX_PORT}/tcp >/dev/null
  this_ssh_port="$(cat /etc/ssh/sshd_config | grep "Port " | cut -d ' ' -f2)"
  firewall-cmd --permanent --zone=public --add-port=${this_ssh_port}/tcp >/dev/null
  firewall-cmd --reload
}


function setup_ubuntu() {
  sudo add-apt-repository universe
  apt update
  if [[ ! -x "$(command -v curl)" ]]; then
    apt install curl -y
  fi
  if [[ ! -x "$(command -v python3)" ]]; then
    apt install python3 -y
  fi
  if [[ ! -x "$(command -v pip3)" ]]; then
    if [[ "$(lsb_release -a | grep Release | awk '{print $2}' | cut -d . -f1 )" == "18" ]]; then
      apt install python3-distutils -y
    else
      apt install python3-distutils-extra -y
    fi
  fi
  if [[ ! "$(dpkg -s python3-dev 2>/dev/null)" ]]; then
    apt install python3-dev -y
  fi
  if [[ ! "$(dpkg -s libpcre3-dev 2>/dev/null)" ]]; then
    apt install libpcre3 libpcre3-dev -y
  fi
  if [[ ! -x "$(command -v nginx)" ]]; then
    apt install nginx -y
  fi
  if [[ ! -x "$(command -v gcc)" ]]; then
    apt install gcc -y
  fi
  ufw allow "${NGINX_PORT}" >/dev/null
  ufw_status="$(sudo ufw status | cut -d ' ' -f2)"
  if [[ "${ufw_status}" == "inactive" ]]; then
    this_ssh_port="$(cat /etc/ssh/sshd_config | grep "Port " | cut -d ' ' -f2)"
    ufw allow "${this_ssh_port}" >/dev/null
    echo 'y' | ufw enable
  fi
}

function check_distro() {
  if [[ -f /etc/os-release ]]; then
  . /etc/os-release
  DISTRO="${ID}"
  if [[  "${DISTRO}" == "ubuntu" || "${DISTRO}" == "centos" ]]; then
      if [[ "${DISTRO}" == "ubuntu" ]]; then
        setup_ubuntu
      else
        setup_centos
      fi
  else echo "Only tested on ubuntu and centos... exiting"
  exit 1
  fi
  else
    echo "Could not determine distro ... exiting"
    exit 1
  fi
}


function make_service() {
  cat >"/etc/systemd/system/keys_server.service" <<EOL
[Unit]
Description=uWSGI instance to serve keys_server
After=network.target

[Service]
User=${SERVICE_USER}
WorkingDirectory=${FLASK_APP_FOLDER}
Environment="PATH=${FLASK_APP_FOLDER}/.venv/bin:${PATH}"
ExecStart=${FLASK_APP_FOLDER}/.venv/bin/uwsgi --ini uwsgi.ini

[Install]
WantedBy=multi-user.target
EOL
}


function make_requirements_file() {
  cat >"${FLASK_APP_FOLDER}/requirements.txt" <<EOL
cryptography==2.4.2
Flask==1.0.2
gevent==1.4.0
greenlet==0.4.15
paramiko==2.4.2
python-dotenv==0.10.1
requests==2.21.0
uWSGI==2.0.17.1
sshpubkeys==3.1.0
EOL
}


function make_uwsgi_ini() {
cat >"${FLASK_APP_FOLDER}/uwsgi.ini" <<EOL
[uwsgi]
wsgi-file = __main__.py
callable = app
master = true
socket = ${FLASK_APP_FOLDER}/app.sock
chdir = ${FLASK_APP_FOLDER}
venv = ${FLASK_APP_FOLDER}/.venv
chmod-socket = 666
vacuum = true
thunder-lock = true
gevent = 1000
enable-threads = true
die-on-term = true
EOL
}


function make_python_app() {
cat >"${FLASK_APP_FOLDER}/__main__.py" <<EOL
"""Simple Server to handle a single POST request."""
import os
import time
import sys
from threading import Thread
from flask import Flask, abort, request
import requests
import json
from dotenv import load_dotenv
import paramiko
import socket
from sshpubkeys import SSHKey

this_file_dir = os.path.dirname(os.path.realpath(__file__))
env_path = os.path.abspath(os.path.join(this_file_dir, '.env'))
if not os.path.exists(env_path):
    sys.exit("Could not get .env file. Exiting...")
load_dotenv(dotenv_path=env_path)
SHARED_SECRET = os.getenv("SHARED_SECRET", None)
OAUTH_VALIDATION_URL = os.getenv("OAUTH_VALIDATION_URL", None)
if SHARED_SECRET is None or OAUTH_VALIDATION_URL is None:
    sys.exit("Could not get the required variables: SHARED_SECRET, OAUTH_VALIDATION_URL . Exiting...")

app = Flask(__name__)


def _valid_token(access_token):
    data = {"token": access_token}
    headers = {'Content-Type': 'application/json'}
    try:
        validation_response = requests.post(OAUTH_VALIDATION_URL, headers=headers, data=json.dumps(data))
        if validation_response.status_code == 200:
            return True
        return False
    except json.JSONDecodeError:
        return False
    except requests.exceptions.ConnectionError:
        return False


# decorator
def a_sync(target):
    """Short summary.

    :param target: the function to run in async mode.
    :type target: function
    :return: Async wrapper.
    :rtype: function

    """
    def wrapper(*args, **kwargs):
        thr = Thread(target=target, args=args, kwargs=kwargs)
        thr.start()
    return wrapper


# @a_sync # uncomment for async handling of posted key
def _valid_key(posted_key):
    ssh = SSHKey(posted_key)
    try:
        ssh.parse()
        return True
    except InvalidKeyError as err:
        print("Invalid key:", err)
        return False
    except NotImplementedError as err:
        print("Invalid key type:", err)
        return False


@a_sync
def _handle_posted_key(partner, pub_key, ssh_host, ssh_port):
    ssh_user=os.getenv('USER')
    auth_keys_path = "/home/%s/.ssh/authorized_keys" % ssh_user
    cmd_to_run = 'echo %s >> /home/%s/%s;mkdir -p ~/.ssh; ' \\
    'if [[ ! -f "%s" ]]; then ' \\
    'echo "%s" > "%s"; ' \\
    'else if [[ -z \$(cat "%s" | grep "%s") ]]; then ' \\
    'echo "%s" >> "%s"; ' \\
    'fi; ' \\
    'fi;exit 0;' % (pub_key, ssh_user, partner, auth_keys_path, pub_key, auth_keys_path, auth_keys_path, pub_key, pub_key, auth_keys_path)
    print(_valid_ssh(ssh_host, ssh_port, cmd_to_run))


def _valid_ssh(ssh_host, ssh_port, cmd_to_run=None):
    client = None
    try:
        _port = int(ssh_port)
        if isinstance(_port, int) and _port > 0:
            client = paramiko.SSHClient()
            private_key_file = os.path.abspath(os.path.join(os.getenv('HOME'), '.ssh', 'id_rsa'))
            private_key = paramiko.RSAKey.from_private_key_file(private_key_file)
            username = os.getenv('USER')
            # add the server's key to known hosts (if not already)
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(hostname=ssh_host, port=ssh_port, username=username, pkey=private_key, timeout=5)
            if cmd_to_run is not None:
                # Send the command (non-blocking)
                stdin, stdout, stderr = client.exec_command(cmd_to_run)
                # Wait for the command to terminate
                while not stdout.channel.exit_status_ready() and not stdout.channel.recv_ready():
                    time.sleep(1)
                stdout_string = stdout.readlines()
                stderr_string = stderr.readlines()
                return stdout_string, stderr_string
            return True
    except paramiko.AuthenticationException:
        return False
    except paramiko.SSHException:
        return False
    except socket.error:
        return False
    except OSError:
        return False
    except ValueError:
        return False
    finally:
        if client is not None:
            client.close()
    return False


@app.route('/', methods=['GET', 'POST'])
def main_route():
    """If valid data are posted, add the public key to he user's authorized_keys.

    :return: 204 if acceptable, 401 else.

    """
    if request.method == "GET":
        return "", 204
    if not request.json:
        return abort(400)
    auth_header = request.headers.get('Authorization')
    if auth_header is None:
        return abort(401)
    token_parts = auth_header.split(" ")
    parts_len = len(token_parts)
    if parts_len != 2:
        return abort(401)
    access_token = token_parts[1]
    token_type = token_parts[0]
    if str(token_type).lower() != "bearer":
        return abort(401)
    if _valid_token(access_token):
        posted_data = request.get_json(force=True)
        if 'secret' in posted_data and posted_data['secret'] == SHARED_SECRET:
            if 'key' in posted_data and 'ssh' in posted_data and isinstance(posted_data, dict) \
                    and 'host' in posted_data['ssh'] and 'port' in posted_data['ssh']:
                pub_key = str(posted_data['key'])
                valid_key = _valid_key(pub_key)
                if not valid_key:
                    return abort(400, 'Invalid public key')
                ssh_data = posted_data['ssh']
                ssh_host = str(ssh_data['host'])
                ssh_port = ssh_data['port']  # ensure int in _valid_ssh
                valid_ssh = _valid_ssh(ssh_host, ssh_port)
                if not valid_ssh:
                    return abort(400, 'Invalid ssh settings')
                partner="partner_name"
                if 'partner' in posted_data:
                    partner=posted_data['partner']
                _handle_posted_key(partner=partner, pub_key=pub_key, ssh_host=ssh_host, ssh_port=ssh_port)
                return "", 204
            return abort(400)
        return abort(400) if 'secret' in posted_data else abort(401)
    return abort(401)

EOL
}


function prepare_flask_app() {
  if [[ ! -x "$(command -v pip3)" ]]; then
    curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
    python3 get-pip.py
    rm -f get-pip.py
  fi
  if [[ ! -x "$(command -v virtualenv)" ]]; then
      python3 -m pip install virtualenv
  fi
  make_requirements_file
  cat >"${FLASK_APP_FOLDER}/.install.sh" <<EOL
#!/bin/bash
PATH=/usr/local/bin:\${PATH}
if [[ ! -d ${FLASK_APP_FOLDER}/.venv ]]; then
  virtualenv -p python3 ${FLASK_APP_FOLDER}/.venv
fi
. ${FLASK_APP_FOLDER}/.venv/bin/activate
  pip3 install -r "${FLASK_APP_FOLDER}/requirements.txt"
EOL
chmod +x ${FLASK_APP_FOLDER}/.install.sh
chown -R "${SERVICE_USER}" "/home/${SERVICE_USER}"
su ${SERVICE_USER} -c ${FLASK_APP_FOLDER}/.install.sh
rm ${FLASK_APP_FOLDER}/.install.sh
}


function set_nginx() {
  cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.orig
FragmentPath="$(systemctl show -p FragmentPath nginx.service | cut -d= -f2)"
PIDFILE="$(cat ${FragmentPath} | grep PIDFile | cut -d= -f2)"
# workaround for: Failed to read PID from file /run/nginx.pid: Invalid argument
# https://bugs.launchpad.net/ubuntu/+source/nginx/+bug/1581864
mkdir -p ${FragmentPath}.d
printf "[Service]\nExecStartPost=/bin/sleep 0.1\n" >${FragmentPath}.d/override.conf
systemctl daemon-reload
NGINX_LISTEN="${NGINX_PORT} default_server"
  cat >"/etc/nginx/nginx.conf" <<EOL
user ${SERVICE_USER};
worker_processes auto;
pid ${PIDFILE};

events {
	worker_connections 768;
}

http {
	tcp_nopush on;
	tcp_nodelay on;
	keepalive_timeout 65;
	types_hash_max_size 2048;
  server {
    listen ${NGINX_PORT} default_server;
    listen [::]:${NGINX_PORT} default_server;
    location / {
      include uwsgi_params;
      uwsgi_pass unix://${FLASK_APP_FOLDER}/app.sock;
    }
  }
}
EOL
nginx -t
}


function set_service_user() {
  if ! id "${SERVICE_USER}" >/dev/null 2>&1; then
    useradd -d /home/${SERVICE_USER} -s /bin/bash ${SERVICE_USER}
  fi
  if [[  "$(getent passwd ${SERVICE_USER} | cut -d: -f7)" !=  "/bin/bash" ]]; then
    usermod --shell /bin/bash ${SERVICE_USER}
  fi
  if [[ -d /home/${SERVICE_USER}/.cache/pip ]]; then
    rm -rf /home/${SERVICE_USER}/.cache/pip
  fi
  if [[ ! -d /home/${SERVICE_USER}/.ssh ]]; then
    mkdir -p /home/${SERVICE_USER}/.ssh
  fi
  chown -R "${SERVICE_USER}" /home/${SERVICE_USER}
  if [[ ! -f /home/${SERVICE_USER}/.ssh/id_rsa ]]; then
    su ${SERVICE_USER} -c 'ssh-keygen -t rsa -N "" -f /home/'${SERVICE_USER}'/.ssh/id_rsa'
  fi
  if [[ -d "${FLASK_APP_FOLDER}" ]]; then
    rm -rf "${FLASK_APP_FOLDER}"
  fi
  mkdir -p "${FLASK_APP_FOLDER}"
  chown -R "${SERVICE_USER}" "${FLASK_APP_FOLDER}"
  chgrp -R "${SERVICE_USER}" "${FLASK_APP_FOLDER}"
}

check_root
check_distro
set_service_user
set_nginx
systemctl enable nginx
systemctl restart nginx
prepare_flask_app
make_python_app
make_uwsgi_ini
make_service
cp "${HERE}/.env" ${FLASK_APP_FOLDER}/.env
chown -R "${SERVICE_USER}" "${FLASK_APP_FOLDER}"
chgrp -R "${SERVICE_USER}" "${FLASK_APP_FOLDER}"
systemctl daemon-reload
systemctl enable keys_server
systemctl restart keys_server
echo "Make sure you have (ssh-copy-id)? on the expected user@backup_server authorized_keys the entry:"
echo "$(cat /home/${SERVICE_USER}/.ssh/id_rsa.pub)"
echo "Use 'cat /home/${SERVICE_USER}/.ssh/id_rsa.pub' to get this entry"
exit 0
