#!/bin/bash

# strict mode
set -euo pipefail
IFS=$'\n\t'

# the path this script exists
HERE=$(cd -P -- "$(dirname -- "$0")" && pwd -P)

function _clear() {
# clear any temp files for backup
  if [[ -f "${HERE}/.backup.env" ]]; then
    rm "${HERE}/.backup.env"
  fi
  if ls ${HERE}/.files* 1>/dev/null 2>&1; then
    rm .files*
  fi
  if ls ${HERE}/.databases* 1>/dev/null 2>&1; then
    rm .databases*
  fi
}

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
    echo "Got keyboard interrupt..."
    _clear
    exit 1
}

# Make sure we can create the service user and access the backup paths
if [[ "$(whoami)" != "root" ]]; then
    echo "Run as root or with sudo"
    _clear
    exit 1
fi

if [[ ! -x "$(command -v rsync)" ]]; then
  echo "Command rsync is not installed ...."
  echo "I am not authorized to install new packages"
  echo "Please install (apt install rsync|yum install rsync) and retry"
  _clear
  exit 1
fi

cd "${HERE}"

PARTNER="${PARTNER:-partner_name}"
SERVICE_TIMER_NAME="os_backups"  # /etc/systemd/system/os_backups.(timer|service)
TIMER_CALENDAR="Sun *-*-* 00:00:00"  # Every(*-*-*) Sunday at 00:00:00
BACKUP_LOCAL_USER="${BACKUP_LOCAL_USER:-}" # use this user's ssh public key
BACKUPS_FOLDER="${BACKUPS_FOLDER:-/var/backups}"  # where to store the backups
BACKUPS_TO_KEEP="${BACKUPS_TO_KEEP:-1}"  # number of backups to keep
OAUTH_TOKEN_HOST="${OAUTH_TOKEN_HOST:-}" # where to get an access token to send the user's public key
OAUTH_CLIENT_ID="${OAUTH_CLIENT_ID:-}" # 0Auth creds for token
OAUTH_CLIENT_SECRET="${OAUTH_CLIENT_SECRET:-}" # 0Auth creds for token
SHARED_SECRET="${SHARED_SECRET:-}" # Extra security (apart the 0Auth token) between us and the keys proxy
KEYS_SERVER_HTTP_HOST="${KEYS_SERVER_HTTP_HOST:-localhost}" # keys proxy address
KEYS_SERVER_HTTP_PORT="${KEYS_SERVER_HTTP_PORT:-80}"  # keys proxy port
BACKUP_REMOTE_SSH_HOST="${BACKUP_REMOTE_SSH_HOST:-localhost}" # backup server host
BACKUP_REMOTE_SSH_PORT="${BACKUP_REMOTE_SSH_PORT:-22}"  # backup server port
BACKUP_PATH="${BACKUP_PATH:-}" # tmp var, used (and changes) later for files backups


function make_backup_timer() {
# the timer to run every "${TIMER_CALENDAR}"
  cat >"/etc/systemd/system/${SERVICE_TIMER_NAME}.timer" <<EOL
[Unit]
Description=Schedule files and / or database backups

[Timer]
OnCalendar=${TIMER_CALENDAR}
Persistent=true

[Install]
WantedBy=timers.target

EOL
}


function make_backup_service() {
# the service that is triggered by the above timer
this_script="${HERE}/$(basename -- "$0")"
if [[ ! -x "${this_script}" ]]; then chmod +x "${this_script}"; fi
  cat >"/etc/systemd/system/${SERVICE_TIMER_NAME}.service" <<EOL
[Unit]
Description=Run files and / or database backups using rsync over ssh

[Service]
Type=simple
ExecStart=${this_script}

[Install]
WantedBy=multi-user.target

EOL
}


function check_service_timer() {
# check if the timer exists and with the calendar on the json config
  should_create=0
  if [[ ! -f "/etc/systemd/system/${SERVICE_TIMER_NAME}.timer" || ! -f "/etc/systemd/system/${SERVICE_TIMER_NAME}.service" ]]; then
    echo "Timer and service do not exist... Installing..."
    should_create=1
  else
    existingCalendar="$(cat /etc/systemd/system/${SERVICE_TIMER_NAME}.timer | grep "OnCalendar=" | cut -d= -f2)"
    if [[ "${existingCalendar}" != "${TIMER_CALENDAR}" ]]; then
      echo "Timer calendar changed... Recreating..."
      should_create=1
    fi
  fi
  if [[ ${should_create} -eq 1 ]]; then
    make_backup_timer
    make_backup_service
    systemctl daemon-reload
    systemctl enable ${SERVICE_TIMER_NAME}.timer
  fi
}


function check_user() {
# check if the configured service user exists and has a ssh keypair
  if ! id "${BACKUP_LOCAL_USER}" >/dev/null 2>&1; then
    echo "Backup user ${BACKUP_LOCAL_USER} does not exist. Creating the new account...."
    sudo useradd -d /home/${BACKUP_LOCAL_USER} -r -m  -s /bin/bash ${BACKUP_LOCAL_USER}
  fi
  if [[ ! -d /home/${BACKUP_LOCAL_USER}/.ssh ]]; then
    sudo mkdir -p /home/${BACKUP_LOCAL_USER}/.ssh
  fi
  sudo chown -R "${BACKUP_LOCAL_USER}" /home/${BACKUP_LOCAL_USER}
  if [[ ! -f /home/${BACKUP_LOCAL_USER}/.ssh/id_rsa ]]; then
    echo "User ${BACKUP_LOCAL_USER} does not have a ssh keypair. Creating one..."
    sudo su ${BACKUP_LOCAL_USER} -c 'ssh-keygen -t rsa -N "" -f /home/'${BACKUP_LOCAL_USER}'/.ssh/id_rsa'
  fi
}


function can_rsync() {
# test if the service user can rsync to the backup server
  key_path=/home/${BACKUP_LOCAL_USER}/.ssh/id_rsa
  if [[ ! -f "${key_path}" ]]; then
    echo "No key in ${key_path} ... exiting"
    return 1
  fi
  ssh_user="$(fingerprint_id)"
  mkdir -p /tmp/testrsync
  status="$(rsync -avz -e "ssh -o StrictHostKeyChecking=no -oConnectTimeout=5 -o UserKnownHostsFile=/dev/null  -i ${key_path} -p ${BACKUP_REMOTE_SSH_PORT}" --dry-run ${ssh_user}@${BACKUP_REMOTE_SSH_HOST}:/ /tmp/testrsync >/dev/null 2>&1)"
  status=$?
  rm -r /tmp/testrsync
  if [[ ${status} -eq 0 ]]; then
    return 0
  fi
  return 1
}


function send_ssh_public_key() {
  key_path=/home/${BACKUP_LOCAL_USER}/.ssh/id_rsa.pub
  if [[ ! -f ${key_path} ]];then
    echo "public key not found ... exiting" # run can_rsync before this, the key should be there
    _clear
    exit 1
  fi
  if [[ ! "$(ssh-keygen -E md5 -lf ${key_path} | grep MD5)" ]]; then
    # valid: 4096 MD5:a4:a5:f1:80:2b:78:91:d6:fc:89:c0:c7:e7:cb:eb:22 laztoum@laptop.laztoum.com (RSA)
    # invalid: /path/to/file is not a public key file.
    echo "Invalid public key... exiting"
    _clear
    exit 1
  fi
  # first get access_token from vm20
  public_key="$(cat ${key_path})"
  auth_token="$(echo -n "${OAUTH_CLIENT_ID}:${OAUTH_CLIENT_SECRET}" | base64 --wrap=0)"
  jsondata="$(curl -s -X POST "${OAUTH_TOKEN_HOST}" --max-time 30 -H "Content-Type: application/x-www-form-urlencoded" -H "Authorization: Basic ${auth_token}" -d "grant_type=client_credentials")"
  if [[ "$(echo "${jsondata}" |grep -w \"access_token\")" ]]; then
    # then use it to send the user's public key
    access_token="$(echo "$jsondata"|grep -w \"access_token\" | tail -1 | cut -d\" -f4)"
    # check if we can connect to provided host,port
    curl "${KEYS_SERVER_HTTP_HOST}:${KEYS_SERVER_HTTP_PORT}" --max-time 30 -s -f -o /dev/null  || { echo "Could not connect to key server ... exiting"; _clear; exit 1; }
    body="{\"secret\": \"${SHARED_SECRET}\", \"key\": \"${public_key}\", \"partner\":\"${PARTNER}\",\"ssh\": {\"host\": \"${BACKUP_REMOTE_SSH_HOST}\", \"port\": ${BACKUP_REMOTE_SSH_PORT}}}"
#    curl -X POST --max-time 30 -H "Content-Type: application/json" -H "Authorization: Bearer ${access_token}" -d"${body}" -vvvv "${KEYS_SERVER_HTTP_HOST}:${KEYS_SERVER_HTTP_PORT}"
#    exit 1
    response=$(curl --write-out %{http_code} -X POST --max-time 30 -H "Content-Type: application/json" -H "Authorization: Bearer ${access_token}" -d"${body}" --silent --output /dev/null "${KEYS_SERVER_HTTP_HOST}:${KEYS_SERVER_HTTP_PORT}")
    if [[ ${response} -gt 300 ]];then  # should be 204
      echo "Could not send our key... exiting"
      _clear
      exit 1
    fi
    echo "Public key sent to server ... continuing..."
  else
    echo "Could not get access_token... exiting"
    _clear
    exit 1
  fi
}


function make_helper_py() {
  cat >"${HERE}/helper.py" <<EOL
#!/usr/bin/env python
"""
    Backup configuration helper.

Read json configuration and provide environment variables for
file and database backups
"""
import json
import os
import sys
import uuid

this_dir = os.path.dirname(os.path.realpath(__file__))
conf_file = os.path.join(this_dir, 'backup.json')
env_file = os.path.join(this_dir, '.backup.env')
files_env = os.path.join(this_dir, '.files.env')
db_env = os.path.join(this_dir, '.databases.env')


def is_sub_path(parent, test_path):
    _parent = os.path.join(os.path.realpath(parent), '')
    _test_path = os.path.realpath(test_path)
    return os.path.commonprefix([_parent, _test_path]) == _parent


def _write_env(file_path, contents, append=False):
    """Write the file with the environment variables.

    :param file_path: the path of the file to write.
    :type file_path: str
    :param contents: The contents of the file to write
    :type contents: str
    :param append: Whether to append to file or not
    :type append: bool
    :return: None
    :rtype: None

    """
    # open file in write or append mode,
    # use + to create the file if it does not exist
    try:
        if append:
            _file = open(file_path, "a+")
        else:
            _file = open(file_path, "w+")
        _file.write(contents)
        _file.close()
        if not append:
            os.chmod(file_path, 0o755)
    except OSError as e:
        print(e)
        sys.exit(1)


def _exclude_files_backup(path_dict, include_path):
    """

    :param path_dict: get paths to exclude from backup
    :param include_path: the path to include
    :return: the paths to exclude
    :type path_dict: dict
    :type include_path: str
    :rtype: list
    """
    _exclude_paths = []
    if 'EXCLUDE_PATHS' in path_dict and isinstance(path_dict['EXCLUDE_PATHS'], list):
        _paths_to_exclude = path_dict['EXCLUDE_PATHS']
        for _exclude_path in _paths_to_exclude:
            # _exclude_paths.append(_exclude_path)
            # may be relative to include_path or absolute
            if is_sub_path(include_path, _exclude_path) or os.path.exists(os.path.join(include_path, _exclude_path)):
                if is_sub_path(include_path, _exclude_path):
                    _diff = _exclude_path.replace(include_path, '')
                else:
                    _diff = os.path.join(include_path, _exclude_path).replace(include_path, '')
                _to_exclude = _diff[1:] if _diff.startswith('/') else _diff
                _to_exclude = _to_exclude[:-1] if _to_exclude.endswith('/') else _to_exclude
                _exclude_paths.append(_to_exclude)
    if 'EXCLUDE_PATTERNS' in path_dict and isinstance(
            path_dict['EXCLUDE_PATTERNS'], list) and len(path_dict['EXCLUDE_PATTERNS']) > 0:
        _exclude_patterns = path_dict['EXCLUDE_PATTERNS']
        _exclude_paths.append("\n".join(str(_pattern) for _pattern in _exclude_patterns))
    return _exclude_paths


def _file_path_config(path_dict):
    if 'PATH' in path_dict:
        _path = path_dict['PATH']
        path_uuid = uuid.uuid4().hex
        _path_file = os.path.join(this_dir, ".files.%s.env" % path_uuid)
        _valid_path = False
        if os.path.exists(_path):
            _valid_path = True
            _write_env(files_env, "%s\n" % path_uuid, True)
            _env_include = _path
            _write_env(_path_file, 'BACKUP_PATH="%s"\n' % _env_include, False)
        _exclude_paths = []
        if _valid_path:
            _exclude_paths = _exclude_files_backup(path_dict=path_dict, include_path=_path)
        if len(_exclude_paths) > 0:
            _exclude_paths_file = os.path.join(this_dir, ".files.%s_EXCLUDE.env" % path_uuid)
            exclude_string = "\n".join(str(_path) for _path in _exclude_paths)
            _write_env(_exclude_paths_file, exclude_string, False)


def _file_envs(files_dict):
    """Create environment variables for files back.

    :param files_dict: The files backup settings dictionary.
    :type files_dict: dict
    :rtype: None

    """
    if 'ENABLED' in files_dict and type(files_dict['ENABLED']) == bool:
        if not files_dict['ENABLED']:
            _write_env(env_file, 'FILES_ENABLED=0\n', True)
            return
        _write_env(env_file, 'FILES_ENABLED=1\n', True)
        if 'PATHS' in files_dict and isinstance(files_dict['PATHS'], list):
            _paths = files_dict['PATHS']
            for path_dict in _paths:
                _file_path_config(path_dict)


def _add_db_entry(db_dict, db_type):
    if 'DB_HOST' in db_dict and 'DB_NAME' in db_dict and 'DB_PORT' in db_dict:
        if not isinstance(db_dict['DB_PORT'], int):
            return
        db_uuid = uuid.uuid4().hex
        _write_env(".databases.%s.env" % db_type, "%s\n" % db_uuid, True)
        _db_file = os.path.join(this_dir, ".databases.%s.%s.env" % (db_type, db_uuid))
        _write_env(_db_file, 'DB_HOST="%s"\n' % str(db_dict['DB_HOST']), False)
        _write_env(_db_file, 'DB_PORT="%s"\n' % db_dict['DB_PORT'], True)
        _write_env(_db_file, 'DB_NAME="%s"\n' % str(db_dict['DB_NAME']) if db_dict['DB_NAME'] is not None else '', True)
        if 'DB_USER' in db_dict:
            _db_user = str(db_dict['DB_USER']) if db_dict['DB_USER'] is not None else ''
            _write_env(_db_file, 'DB_USER="%s"\n' % _db_user, True)
        if 'DB_PASSWORD' in db_dict:
            _db_password = str(db_dict['DB_PASSWORD']) if db_dict['DB_PASSWORD'] is not None else ''
            _write_env(_db_file, 'DB_PASSWORD="%s"\n' % _db_password, True)
        if 'AUTHENTICATION_DATABASE' in db_dict:
            _authentication_db = str(db_dict['AUTHENTICATION_DATABASE']) \\
                if db_dict['AUTHENTICATION_DATABASE'] is not None else ''
            _write_env(_db_file, 'AUTHENTICATION_DATABASE="%s"\n' % _authentication_db, True)


def _db_envs(db_dict):
    """Collect from configuration database settings for backup.

    :param db_dict: Description of parameter `db_dict`.
    :type db_dict: dict
    :return: Description of returned object.
    :rtype: None

    """
    valid_db_types = ['mongo', 'postgresql', 'mysql', 'mariadb', 'mysql_mariadb']
    if 'ENABLED' in db_dict and isinstance(db_dict['ENABLED'], bool):
        if not db_dict['ENABLED']:
            for db_enabled_str in ['MONGO_ENABLED', 'POSTGRESQL_ENABLED', 'MYSQL_MARIADB_ENABLED']:
                _write_env(env_file, '%s="0"\n' % db_enabled_str, True)
            return
        if 'DATABASES' in db_dict and isinstance(db_dict['DATABASES'], list):
            _databases = db_dict['DATABASES']
            enabled_db_types = []
            for _entry in _databases:
                if 'DB_TYPE' in _entry and str(_entry['DB_TYPE']) in valid_db_types:
                    db_type = str(_entry['DB_TYPE'])
                    if db_type == 'mariadb' or db_type == 'mysql':
                        db_type = 'mysql_mariadb'
                    if db_type not in enabled_db_types:
                        db_enabled_str = "%s_ENABLED" % db_type.upper()
                        _write_env(env_file, '%s="1"\n' % db_enabled_str, True)
                        enabled_db_types.append(db_type)
                    _add_db_entry(_entry, db_type)


def _connection_envs(json_data):
    """Check and write environment variables related to backup settings.

    :type json_data: dict
    :rtype: None

    """
    keys = [
        'OAUTH_TOKEN_HOST',
        'OAUTH_CLIENT_ID',
        'PARTNER',
        'OAUTH_CLIENT_SECRET',
        'SHARED_SECRET',
        'KEYS_SERVER_HTTP_HOST',
        'KEYS_SERVER_HTTP_PORT',
        'TIMER_CALENDAR',
        'BACKUP_LOCAL_USER',
        'BACKUPS_FOLDER',
        'BACKUPS_TO_KEEP',
        'BACKUP_REMOTE_SSH_HOST',
        'BACKUP_REMOTE_SSH_PORT'
    ]
    for key in keys:
        if key in json_data:
            _write_env(env_file, '%s="%s"\n' % (key, json_data[key] if json_data[key] is not None else ''), True)


def _make_envs(json_data):
    _write_env(env_file, '', False)  # init: empty file
    _connection_envs(json_data)
    if 'FILES_BACKUP' in json_data:
        _file_envs(json_data['FILES_BACKUP'])
        # _write_env(os.path.join(this_dir, "files.json"), json.dumps(json_data['FILES_BACKUP']))
    if 'DATABASE_BACKUP' in json_data:
        _db_envs(json_data['DATABASE_BACKUP'])
        # _write_env(os.path.join(this_dir, "databases.json"), json.dumps(json_data['DATABASE_BACKUP'], indent=4))


def main():
    """Parse configuration json and prepare environment variables.

    :return: None
    :rtype: None
    """
    json_data = {}
    if os.path.exists(conf_file) and os.path.isfile(conf_file):
        _data = open(conf_file).read()
        try:
            json_data = json.loads(_data)
        except json.decoder.JSONDecodeError as e:
            print(e)
            sys.exit(1)
        except ValueError as e:
            print(e)
            sys.exit(1)
        finally:
            if len(json_data.keys()) == 0:
                print("No json data... Exiting")
                sys.exit(1)
    _make_envs(json_data)


if __name__ == '__main__':
    main()
EOL

}


function make_config_example() {
  cat >"${HERE}/backup.json.example" <<EOL
{
  "TIMER_CALENDAR": "Sun *-*-* 00:00:00",
  "BACKUPS_FOLDER": "/backups",
  "BACKUPS_TO_KEEP": 3,
  "PARTNER": "UWA",
  "BACKUP_LOCAL_USER": "backer",
  "KEYS_SERVER_HTTP_HOST":"http://XXX.XXX.XXX.XXX",
  "KEYS_SERVER_HTTP_PORT":8089,
  "BACKUP_REMOTE_SSH_HOST":"YYY.YYY.YYY.YYY",
  "BACKUP_REMOTE_SSH_PORT":2244,
  "OAUTH_TOKEN_HOST":"https://example.com/oauth/token",
  "OAUTH_CLIENT_ID" : "replacewithoauthclientid",
  "OAUTH_CLIENT_SECRET" : "replacewithoauthclientsecret",
  "SHARED_SECRET": "replacewithsharedservicesecret",
  "FILES_BACKUP": {
    "ENABLED": true,
    "PATHS": [{
      "PATH": "/var/something",
      "EXCLUDE_PATHS": [
        "/var/something/to/exclude1",
        "/var/something/to/exclude2"
      ],
      "EXCLUDE_PATTERNS" : [
        "*/.idea",
        "*/.vscode"
      ]
    },{
      "PATH": "/path/to/include",
      "EXCLUDE_PATHS": [],
      "EXCLUDE_PATTERNS" : [
        "*/.git",
        "*.sock",
        "*.lock",
        "*/*cache*",
        "*/node_modules"
      ]
    }]
  },
  "DATABASE_BACKUP": {
	  "ENABLED": true,
	  "DATABASES": [{
      "DB_TYPE": "mongo|postgresql|mysql|mariadb",
      "DB_HOST":"127.0.0.1",
      "DB_PORT":27017|5432|3306|...,
      "DB_USER": "admin",
      "DB_PASSWORD": null,
      "DB_NAME": "leave_empty_or_set_null_for_all",
      "AUTHENTICATION_DATABASE": "if_mongo_and_auth_else_remove_it_leave_it_empty_or_set_null"
		}]
	}
}

EOL
}


function backup_folder_structure() {
  if [[ ! -d "${BACKUPS_FOLDER}" ]]; then
    mkdir -p "${BACKUPS_FOLDER}"
  fi
  THIS_BACKUP_FOLDER_NAME="$(date +"%Y_%m_%d__%H_%M_%S")"
  mkdir -p "${BACKUPS_FOLDER}/${THIS_BACKUP_FOLDER_NAME}"
  folders_count="$(ls -l "${BACKUPS_FOLDER}" | grep -c ^d)"
  if [[ "${folders_count}" -gt "$((BACKUPS_TO_KEEP))" ]]; then
#    echo "Found "$((folders_count - 1))" folders, keeping only the latest ${BACKUPS_TO_KEEP}"
    folders_in_dir=($(ls -t "${BACKUPS_FOLDER}"))  # order by time revers
    COUNTER=0
    for folder in "${folders_in_dir[@]}"; do
      if [[ -d "${BACKUPS_FOLDER}/${folder}" ]];then
        COUNTER=$((COUNTER + 1))
        if [[ "${COUNTER}" -gt "${BACKUPS_TO_KEEP}" ]]; then
          rm -rf ${BACKUPS_FOLDER}/${folder}
        fi
      fi
    done
  fi
  if [[ -f "${HERE}/files.tgz" ]];then
    mv "${HERE}/files.tgz" "${BACKUPS_FOLDER}/${THIS_BACKUP_FOLDER_NAME}/files.tgz"
  fi
  if [[ -f "${HERE}/databases.tgz" ]];then
    mv "${HERE}/databases.tgz" "${BACKUPS_FOLDER}/${THIS_BACKUP_FOLDER_NAME}/databases.tgz"
  fi
}


function files_backup() {
  file_dirs=()
  if [[ -f "${HERE}/.files.env" ]]; then
    while IFS= read -r item_id
    do
      if [[ -f "${HERE}/.files.${item_id}.env" ]]; then
        . "${HERE}/.files.${item_id}.env"
        x="$(basename "${BACKUP_PATH}")"
        BACKUP_NAME="$(echo "${BACKUP_PATH}" | sed 's|/|__ASD__|g')".tgz
        rm "${HERE}/.files.${item_id}.env"
        chdir_path=$(dirname "${BACKUP_PATH}")
        if [[ -f "${HERE}/.files.${item_id}_EXCLUDE.env" ]]; then
          tar -cpzf "${BACKUP_NAME}" -X "${HERE}/.files.${item_id}_EXCLUDE.env" -C "${chdir_path}" "$(basename "${BACKUP_PATH}")"
          rm "${HERE}/.files.${item_id}_EXCLUDE.env"
        else
          tar -cpzf "${BACKUP_NAME}" -C "${chdir_path}" "$(basename "${BACKUP_PATH}")"
        fi
        file_dirs+=("${BACKUP_NAME}")
      fi
    done < "${HERE}/.files.env"
  fi
  if [[ -d "${HERE}/files" ]]; then
    rm -rf "${HERE}/files"
  fi
  if [[ "${#file_dirs[@]}" -gt 0 ]];then
    mkdir -p "${HERE}/files"
    for gz in "${file_dirs[@]}"; do
      if [[ -f "${HERE}/${gz}" ]]; then
         mv "${HERE}/${gz}" "${HERE}/files"
      fi
    done
    tar -cpzf "files.tgz" -C "${HERE}" "files"
  fi
  if [[ -d "${HERE}/files" ]]; then
    rm -rf "${HERE}/files"
  fi
}


function db_backup() {
  DB_HOST="${DB_HOST:-localhost}"
  DB_PORT="${DB_PORT:-}"
  DB_USER="${DB_USER:-admin}"
  DB_PASSWORD="${DB_PASSWORD:-}"
  DB_NAME="${DB_NAME:-}"
  AUTHENTICATION_DATABASE="${AUTHENTICATION_DATABASE:-}"
  db_dumps=()
  dbtype="${1:-}";
  cmd_args=()
  can_dump=0
  if [[ "${dbtype}" == "mongo" || "${dbtype}" == "mysql_mariadb"  || "${dbtype}" == "postgresql" ]]; then
    if [[ "${dbtype}" == "mongo" && ! -x "$(command -v mongodump)" ]]; then
        echo "mongodump command not found... Skipping"
        can_dump=1
    fi
    if [[ "${can_dump}" -eq 0 && "${dbtype}" == "postgresql" && ! -x "$(command -v pg_dump)" ]]; then
      echo "pg_dump command not found... Skipping"
      can_dump=1
    fi
    if [[ "${can_dump}" -eq 0 && "${dbtype}" == "mysql_mariadb" && ! -x "$(command -v mysqldump)" ]]; then
      echo "mysqldump command not found... Skipping"
      can_dump=1
    fi
    if [[ "${can_dump}" -eq 0 ]]; then
      if [[ -f "${HERE}/.databases.${dbtype}.env" ]]; then
        while IFS= read -r item_id; do
          _env_file="${HERE}/.databases.${dbtype}.${item_id}.env"
          . "${_env_file}"
          if [[ ! -z "${DB_PORT}" && ! -z "${DB_HOST}"  ]]; then
            if [[ -z "${DB_NAME}" ]] ; then
              dump_name="${dbtype}_all_${dbtype}.dump.gz"
            else
              dump_name="${dbtype}_${DB_NAME}.dump.gz"
            fi
            cmd_args=(--host="${DB_HOST}" --port="${DB_PORT}")
            # --host="${DB_HOST}" --port="${DB_PORT}"
            cmd_out="$(echo 1)"
            if [[ "${dbtype}" == "mongo" ]]; then
              cmd_args+=(--gzip)
#              cmd_args+=(--gzip --quiet)
              if [[ ! -z "${DB_NAME}" ]]; then
                cmd_args+=(--db="${DB_NAME}")
              fi
              if [[ ! -z "${DB_USER}" && ! -z "${DB_PASSWORD}" ]]; then
                cmd_args+=(--username="${DB_USER}")
                cmd_args+=(--password="${DB_PASSWORD}")
              fi
              if [[ ! -z "${AUTHENTICATION_DATABASE}" ]]; then
                cmd_args+=(--authenticationDatabase="${AUTHENTICATION_DATABASE}")
              fi
              cmd_args+=(--archive="${dump_name}")
              echo "${cmd_args[@]}"
              mongodump  -vvvvv "${cmd_args[@]}"
              cmd_out="$(mongodump "${cmd_args[@]}" || echo 1)"
            elif [[ "${dbtype}" == "postgresql" ]]; then
              if [[ ! -z "${DB_NAME}" ]]; then  # case: specific db
                if [[ ! -z "${DB_USER}" && ! -z "${DB_PASSWORD}" ]]; then  # specific db and user (password should also be specified)
                  cmd_out="$(PGPASSWORD="${DB_PASSWORD}" pg_dump --host="${DB_HOST}" --port="${DB_PORT}" --username="${DB_USER}" -Fc "${DB_NAME}" > ${dump_name}|| echo 1)"
                else # no host or port => password will be asked, run it as postgres user
                  cmd_out="$(sudo -u postgres -H pg_dump -Fc "${DB_NAME}" > ${dump_name}|| echo 1)"
                fi
              else
                # dump all
                cmd_out="$(sudo -u postgres -H pg_dumpall --clean --if-exists | gzip > ${dump_name} || echo 1)"
              fi
            elif [[ "${dbtype}" == "mysql_mariadb" ]]; then
              cmd_args+=(--hex-blob --routines --triggers)
              if [[ ! -z "${DB_USER}" ]]; then
                cmd_args+=(--user="${DB_USER}")
              fi
              if [[ ! -z "${DB_PASSWORD}" ]]; then
                cmd_args+=(--password="${DB_PASSWORD}")
              fi
              if [[ ! -z "${DB_NAME}" ]]; then
                cmd_args+=(--databases "${DB_NAME}")
              else
                cmd_args+=(--all-databases)
              fi
              cmd_out="$(mysqldump "${cmd_args[@]}" | gzip > ${dump_name} || echo 1)"
            fi
            if [[ ! -z ${cmd_out} ]]; then
              echo "Could not dump on ${DB_HOST}:${DB_PORT}/${DB_NAME}... skipping"
            else
              db_dumps+=("${dump_name}")
            fi
          fi
        done  < "${HERE}/.databases.${dbtype}.env"
      fi
    fi
  fi
  if [[ "${#db_dumps[@]}" -gt 0 ]];then
    mkdir -p "${HERE}/databases"
    for dump in "${db_dumps[@]}"; do
      if [[ -f "${HERE}/${dump}" ]]; then
        mv "${HERE}/${dump}" "${HERE}/databases/${dump}"
      else
        echo "${HERE}/${dump}"
      fi
    done
  fi
}


function fingerprint_id() {
  # should be run after we make sure we can ssh
  ssh-keygen -lf /home/${BACKUP_LOCAL_USER}/.ssh/id_rsa.pub -E md5 | awk '{print $2}' | sed 's/://g' | sed 's/MD5//g'
  # transform: 4096 MD5:a4:a5:f1:80:2b:78:91:d6:fc:89:c0:c7:e7:cb:eb:22 user@host.domain (RSA)
  # to id: a4a5f1802b7891d6fc89c0c7e7cbeb22
}


function do_rsync() {
  # the actual rsync with hte backup server
  if ls ${HERE}/files.tgz 1>/dev/null 2>&1  || ls ${HERE}/databases.tgz 1>/dev/null 2>&1; then
    backup_folder_structure
    items_in_folder="$(ls -l "${BACKUPS_FOLDER}" | grep -c ^d)"
    if [[ ${items_in_folder} -gt 0 ]]; then
      rsync -avz --progress --delete  -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -p${BACKUP_REMOTE_SSH_PORT} -i /home/${BACKUP_LOCAL_USER}/.ssh/id_rsa" ${BACKUPS_FOLDER}/* "$(fingerprint_id)"@"${BACKUP_REMOTE_SSH_HOST}":/
      cp "${HERE}/backup.json" "${HERE}/restore_${THIS_BACKUP_FOLDER_NAME}.json"
    else
      echo "Backup folder is empty... Exiting"
      _clear
      exit 1
    fi
  else
    echo "No files or database backup is created... Exiting"
    _clear
    exit 1
  fi
}


function _check_backups(){
  # Check the environment variables and if enabled, do the files and / or databases archives
  enabled_items=0
  FILES_ENABLED="${FILES_ENABLED:-0}"
  if [[ "${FILES_ENABLED}" -eq 0 ]]; then
    echo "Files backup is not enabled. Skipping..."
  else
    files_backup
    enabled_items=$((enabled_items + 1))
  fi
  MONGO_ENABLED="${MONGO_ENABLED:-0}"
  if [[ "${MONGO_ENABLED}" -eq 0 ]]; then
    echo "Mongodb backup is not enabled. Skipping..."
  else
#    mongo_backup
    db_backup "mongo"
    enabled_items=$((enabled_items + 1))
  fi
  POSTGRESQL_ENABLED="${POSTGRESQL_ENABLED:-0}"
  if [[ "${POSTGRESQL_ENABLED}" -eq 0 ]]; then
    echo "Postgresql backup is not enabled. Skipping..."
  else
#    postgresql_backup
    db_backup "postgresql"
    enabled_items=$((enabled_items + 1))
  fi
  MYSQL_MARIADB_ENABLED="${MYSQL_MARIADB_ENABLED:-0}"
  if [[ "${MYSQL_MARIADB_ENABLED}" -eq 0 ]]; then
    echo "Mysql / mariadb backup is not enabled. Skipping..."
  else
#    mysql_mariadb_backup
    db_backup "mysql_mariadb"
    enabled_items=$((enabled_items + 1))
  fi
  if [[ -d "${HERE}/databases" ]];then
     tar -cpzf "databases.tgz" -C "${HERE}" "databases"
     rm -rf "${HERE}/databases"
  fi
  if [[ "${enabled_items}" -gt 0 ]]; then
    do_rsync
  else
    echo "No files or database backup is enabled... Exiting"
    _clear
    exit 0
  fi
}


function _main() {

# the actual backup flow
if [[ -f "${HERE}/.backup.env" ]]; then
  # source the generated .env
  . "${HERE}/.backup.env"
  check_user
  check_service_timer
  echo "Checking if we can rsync to the backup server ..."
  if ! can_rsync; then
    echo "We cannot rsync to backup server... sending our key to keys proxy"
    send_ssh_public_key
    COUNTER=0
    # at most ten retries to ssh
    while [[ ${COUNTER} -lt 10 ]]; do
      sleep 1
      echo "Retrying rsync ...."
      if can_rsync; then
        COUNTER=$((COUNTER+10))
      else
        COUNTER=$((COUNTER+1))
      fi
    done
    if ! can_rsync; then
      echo "We still cannot rsync ... :( exiting"
      exit 1
    fi
  fi
  echo "We can rsync ... "
  echo "Continuing with backup ..."
  _check_backups
else
  echo "No .backup.env file found. Something went wrong..."
  _clear
  exit 1
fi
echo "Backup complete"
_clear
}

_clear

# check for backup.json with backup settings
if [[ ! -f "${HERE}/backup.json" ]]; then
  echo "No backup.json file found"
  if [[ ! -f "${HERE}/backup.json.example" ]]; then
    make_config_example
  fi
  echo "use: ";
  echo "cp ${HERE}/backup.json.example ${HERE}/backup.json"
  echo "edit its contents and retry"
  _clear
  exit 1
fi

# if helper.py does not exist, create it
if [[ ! -f "${HERE}/helper.py" ]]; then
  make_helper_py
fi

if [[ -x "$(command -v python)" ]]; then
  sed -i 's|#!/usr/bin/env .*|#!/usr/bin/env python|g' "${HERE}/helper.py"
  elif [[ -x "$(command -v python3)" ]]; then
    sed -i 's|#!/usr/bin/env .*|#!/usr/bin/env python3|g' "${HERE}/helper.py"
  elif  [[ -x "$(command -v python2)" ]]; then
    sed -i 's|#!/usr/bin/env .*|#!/usr/bin/env python2|g' "${HERE}/helper.py"
fi
if [[ ! -x "${HERE}/helper.py" ]]; then
  chmod +x "${HERE}/helper.py"
fi

# call helper.py to parse backup.json and
# generate .env files with variables for backup
${HERE}/helper.py

# check .env files and do backup
_main

exit 0
