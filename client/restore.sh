#!/bin/bash

# strict mode
set -euo pipefail
IFS=$'\n\t'

# the path this script exists
HERE=$(cd -P -- "$(dirname -- "$0")" && pwd -P)


function _clear() {
  if [[ -d "${HERE}/.tmp" ]]; then
#    ls "${HERE}/.tmp"
    rm -rf "${HERE}/.tmp"
  fi
  if [[ -f "${HERE}/.backup.env" ]]; then
    rm "${HERE}/.backup.env"
  fi
  if ls ${HERE}/.files* 1>/dev/null 2>&1; then
    rm .files*
  fi
  if ls ${HERE}/.databases* 1>/dev/null 2>&1; then
    rm .databases*
  fi
}

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT


function ctrl_c() {
    echo "Got keyboard interrupt..."
    _clear
    exit 1
}


function _restore_files() {
  tar xf "${RESTORE_FOLDER}/files.tgz" -C "${HERE}/.tmp"
  if [[ -d "${HERE}/.tmp/files" ]]; then
    FILES_FOLDER_CONTENTS=( $(ls "${HERE}/.tmp/files" ) )
    if [[ "${#FILES_FOLDER_CONTENTS[@]}" -ne 0 ]]; then
      for folder in "${FILES_FOLDER_CONTENTS[@]}"; do
        folder_path="$(echo "${folder}" | sed 's|__ASD__|/|g' | sed 's|.tgz||')"
        mkdir -p "${folder_path}"
#        mkdir -p "${HERE}/.tmp/files/${folder_path}" && ls "${HERE}/.tmp/files/${folder_path}"
        tar xfp "${HERE}/.tmp/files/${folder}" -C "$(dirname "${folder_path}")"
      done
    fi
  fi
}


function _restore_database() {
  dump_path="$1"
  db_type="$2"
  DB_NAME="${DB_NAME:-}"
  DB_TYPE="${DB_TYPE:-}"
  DB_PORT="${DB_PORT:-}"
  DB_HOST="${DB_HOST:-}"
  DB_USER="${DB_USER:-}"
  DB_PASSWORD="${DB_PASSWORD:-}"
  AUTHENTICATION_DATABASE="${AUTHENTICATION_DATABASE:-}"
  if [[  "${db_type}" == "${DB_TYPE}" && -f "${dump_path}" ]]; then
    echo "Restoring: ${DB_HOST}:${DB_PORT}/${DB_NAME}"
    cmd_args=(--host="${DB_HOST}" --port="${DB_PORT}")
    cmd_out="$(echo 1)"
    if [[ "${db_type}" == "mongo" ]]; then
      cmd_args+=(--gzip --quiet)
      if [[ ! -z "${DB_NAME}" ]]; then
        cmd_args+=(--db="${DB_NAME}")
      fi
      if [[ ! -z "${DB_USER}" && ! -z "${DB_PASSWORD}" ]]; then
        cmd_args+=(--username="${DB_USER}")
        cmd_args+=(--password="${DB_PASSWORD}")
      fi
      if [[ ! -z "${AUTHENTICATION_DATABASE}" ]]; then
        cmd_args+=(--authenticationDatabase="${AUTHENTICATION_DATABASE}")
      fi
      cmd_args+=(--archive="${dump_path}")
      cmd_out="$(mongorestore "${cmd_args[@]}" || echo 1)"
    elif [[ "${db_type}" == "postgresql" ]]; then # a beautiful mess
      if [[ ! -z "${DB_NAME}" ]]; then  # case: specific db
        sudo -u postgres -H -- psql -c "DROP DATABASE IF EXISTS ${DB_NAME};"
        sudo -u postgres -H -- psql -c "CREATE DATABASE ${DB_NAME};"
        if [[ ! -z "${DB_USER}" && ! -z "${DB_PASSWORD}" ]]; then  # specific db and user (password should also be specified)
         cmd_out="$(PGPASSWORD="${DB_PASSWORD}" pg_restore --host="${DB_HOST}" --port="${DB_PORT}" --username="${DB_USER}" --dbname="${DB_NAME}" "${dump_path}" >/dev/null 2>&1 || echo 1)"
        else
         cmd_out="$(sudo -u postgres -H pg_restore --dbname="${DB_NAME}" "${dump_path}" >/dev/null 2>&1 || echo 1)"
        fi
      else # all dbs
        cmd_out="$(gunzip -c "${dump_path}" | egrep -v '^(CREATE|DROP) ROLE(.*)postgres;' | sudo -u postgres -H psql >/dev/null 2>&1 || echo 1)"
#         cmd_out="$(gunzip -c "${dump_path}" | egrep -v '^(CREATE|DROP) ROLE(.*)postgres;' | sudo -u postgres -H psql || echo 1)"
      fi
    elif [[ "${db_type}" == "mysql" ]]; then
      if [[ ! -z "${DB_NAME}" ]]; then
        cmd_args+=(-D "${DB_NAME}")
      fi
      if [[ ! -z "${DB_USER}" ]]; then
        cmd_args+=(-u "${DB_USER}")
      fi
      if [[ ! -z "${DB_PASSWORD}" ]]; then
        cmd_args+=(-p"${DB_PASSWORD}")
      fi
      cmd_out="$(gunzip < "${dump_path}" | mysql "${cmd_args[@]}" || echo 1)"
    fi
    if [[ ! -z ${cmd_out} ]]; then
      echo "Could not restore: ${DB_HOST}:${DB_PORT}/${DB_NAME}... skipping"
    else
      echo "Restored: ${DB_HOST}:${DB_PORT}/${DB_NAME}"
    fi
  fi
}


function _restore_databases() {
  tar xf "${RESTORE_FOLDER}/databases.tgz" -C "${HERE}/.tmp"
  if [[ -d "${HERE}/.tmp/databases" ]]; then
    DATABASES_FOLDER_CONTENTS=( $(ls "${HERE}/.tmp/databases" ) )
    if [[ "${#DATABASES_FOLDER_CONTENTS[@]}" -ne 0 ]]; then
      for dump in "${DATABASES_FOLDER_CONTENTS[@]}"; do
        db_type="$(echo "${dump}" | cut -d _ -f1)"
        if [[ "${db_type}" == "mongo" || "${db_type}" == "mysql"  || "${db_type}" == "postgresql" ]]; then
          _db_type_filter="x['DB_TYPE']=='${db_type}'"
          if [[ "${db_type}" == "mysql" ]]; then
            db_type="mysql_mariadb";
            _db_type_filter="(x['DB_TYPE']=='mysql' or x['DB_TYPE']=='mariadb')"
          fi
          db_name="$(echo "${dump}" | sed 's|.dump.gz||' | sed "s|${db_type}_||")"
          _db_name_filter="x['DB_NAME']=='${db_name}'"
          if [[ "${db_name}" == "all_${db_type}" ]]; then
            _db_name_filter="(x['DB_NAME']=='' or x['DB_NAME'] is None)"
          fi
          json_file='backup.json'
          _basename="$(basename "restore_${RESTORE_FOLDER}.json")"
          if [[ -f "${_basename}" ]]; then
            json_file="${_basename}"
          fi
          cat >"${HERE}/.tmp/databases/.py_env.py" <<EOL
import os
import json
import sys
conf_file='${HERE}/${json_file}'
write_path='${HERE}/.tmp/databases/.py.env'
if os.path.exists(conf_file) and os.path.isfile(conf_file):
    _data = open(conf_file).read()
    try:
        json_data = json.loads(_data)
        if 'DATABASE_BACKUP' in json_data:
            db_entry_l = list(filter(lambda x: ${_db_type_filter} and ${_db_name_filter}, json_data['DATABASE_BACKUP']['DATABASES']))
            if len(db_entry_l) > 0:
                entry=db_entry_l[0]
                _env_str=""
                for key,val in entry.items():
                    _val = val
                    if str(key) == 'DB_TYPE' and str(val) == 'mariadb':
                        _val = 'mysql'
                    _env_str += '%s="%s"\n' % (str(key),str(_val) if _val is not None else '')
                _file = open(write_path, "w+")
                _file.write(_env_str)
                _file.close()
    except json.decoder.JSONDecodeError as e:
        print(e)
        sys.exit(1)
    except ValueError as e:
        print(e)
        sys.exit(1)
    except OSError as e:
        print(e)
        sys.exit(1)
    finally:
        sys.exit(0)
EOL
          if [[ -f "${HERE}/.tmp/databases/.py.env" ]]; then
            rm "${HERE}/.tmp/databases/.py.env"
          fi
          if [[ -x "$(command -v python)" ]]; then
            python ${HERE}/.tmp/databases/.py_env.py
            elif [[ -x "$(command -v python3)" ]]; then
            python3 ${HERE}/.tmp/databases/.py_env.py
            elif  [[ -x "$(command -v python2)" ]]; then
            python2 ${HERE}/.tmp/databases/.py_env.py
          fi
          rm ${HERE}/.tmp/databases/.py_env.py
          if [[ -f "${HERE}/.tmp/databases/.py.env" ]]; then
            . "${HERE}/.tmp/databases/.py.env"
            if [[ "${db_type}" == "mysql_mariadb" ]]; then
              db_type="mysql"
            fi
            _restore_database "${HERE}/.tmp/databases/${dump}" "${db_type}"
          fi
        fi
      done
    fi
  fi
}
if [[ $# -ne 1 ]]; then
  echo "Usage: ${0} /path/to/directory"
  _clear
  exit 1
fi

RESTORE_FOLDER="${1}"

if [[ ! -d "${RESTORE_FOLDER}" ]]; then
  echo "Invalid directory"
  _clear
  exit 1
fi

if [[ "$(whoami)" != "root" ]]; then
    echo "Run as root or with sudo"
    _clear
    exit 1
fi

RESTORE_FOLDER_CONTENTS=( $(ls "${RESTORE_FOLDER}" ) )
if [[ "${#RESTORE_FOLDER_CONTENTS[@]}" -eq 0 ]]; then
  echo "Empty dir..."
  _clear
  exit 1
fi

cd "${HERE}"
mkdir -p "${HERE}/.tmp"
if [[ -f "${RESTORE_FOLDER}/files.tgz" ]]; then
  _restore_files
fi
if [[ -f "${RESTORE_FOLDER}/databases.tgz" ]]; then
  _restore_databases
fi
_clear
